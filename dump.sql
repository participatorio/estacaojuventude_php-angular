
[36mWelcome to CakePHP v2.5.6 Console[0m
---------------------------------------------------------------
App : app
Path: /home/humberto/Sites/estacao/app/
---------------------------------------------------------------
Cake Schema Shell
---------------------------------------------------------------


DROP TABLE "public"."estados";
DROP TABLE "public"."grupos";
DROP TABLE "public"."localizacoes";
DROP TABLE "public"."municipios";
DROP TABLE "public"."ocorrencias_municipais";
DROP TABLE "public"."orgao_executor";
DROP TABLE "public"."programa_orgaos_executores";
DROP TABLE "public"."programas";
DROP TABLE "public"."programas_tematicas";
DROP TABLE "public"."situacoes";
DROP TABLE "public"."status";
DROP TABLE "public"."tematicas";
DROP TABLE "public"."usuarios";


CREATE TABLE "public"."estados" (
	"id" serial NOT NULL,
	"nome" varchar(255) DEFAULT NULL,
	"sigla" varchar(255) DEFAULT NULL,
	"latitude" float DEFAULT NULL,
	"longitude" float DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."grupos" (
	"id" serial NOT NULL,
	"nome" varchar(100) DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."localizacoes" (
	"id" bigserial NOT NULL,
	"email" varchar(255) DEFAULT NULL,
	"endereco" text DEFAULT NULL,
	"horario_funcionamento" varchar(255) DEFAULT NULL,
	"local" text DEFAULT NULL,
	"nome_referencia" varchar(255) DEFAULT NULL,
	"quantidade_vagas" integer DEFAULT NULL,
	"telefone" varchar(255) DEFAULT NULL,
	"version" integer DEFAULT NULL,
	"ocorrencia_municipal_id" integer DEFAULT NULL,
	"situacao" varchar(255) DEFAULT NULL,
	"status" varchar(255) DEFAULT NULL,
	"ultima_alteracao" date DEFAULT NULL,
	"utilizador_id" integer DEFAULT NULL,
	"status_id" integer DEFAULT NULL,
	"situacao_id" integer DEFAULT NULL,
	"latitude" float DEFAULT NULL,
	"longitude" float DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."municipios" (
	"id" bigserial NOT NULL,
	"nome" varchar(255) DEFAULT NULL,
	"uf" bigint DEFAULT NULL,
	"ultima_alteracao" date DEFAULT NULL,
	"latitude" float DEFAULT NULL,
	"longitude" float DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."ocorrencias_municipais" (
	"id" serial NOT NULL,
	"beneficios_locais" text DEFAULT NULL,
	"como_acessar" text DEFAULT NULL,
	"fim_inscricoes" varchar(255) DEFAULT NULL,
	"inicio_inscricoes" varchar(255) DEFAULT NULL,
	"quantidade_vagas" integer DEFAULT NULL,
	"status" varchar(255) DEFAULT NULL,
	"municipio_id" integer DEFAULT NULL,
	"programa_id" integer DEFAULT NULL,
	"usuario_id" integer DEFAULT NULL,
	"situacao" varchar(255) DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."orgao_executor" (
	"id" bigserial NOT NULL,
	"contato" varchar(255) DEFAULT NULL,
	"endereco" text DEFAULT NULL,
	"nome" varchar(255) DEFAULT NULL,
	"site" varchar(255) DEFAULT NULL,
	"version" integer DEFAULT NULL,
	"ultima_alteracao" date DEFAULT NULL,
	"sigla" varchar(255) DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."programa_orgaos_executores" (
	"programa_id" integer NOT NULL,
	"orgao_executor_id" integer NOT NULL,
	"id" serial NOT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."programas" (
	"id" serial NOT NULL,
	"beneficios" text DEFAULT NULL,
	"criterios_acesso" text DEFAULT NULL,
	"data_inicio" date DEFAULT NULL,
	"descricao" text DEFAULT NULL,
	"duracao" text DEFAULT NULL,
	"idade_maxima" integer DEFAULT NULL,
	"idade_minima" integer DEFAULT NULL,
	"lei_criacao" varchar(255) DEFAULT NULL,
	"nivel" varchar(255) DEFAULT NULL,
	"nome_divulgacao" varchar(255) DEFAULT NULL,
	"nome_oficial" varchar(255) DEFAULT NULL,
	"objetivos" text DEFAULT NULL,
	"publico_alvo" text DEFAULT NULL,
	"sigla" varchar(255) DEFAULT NULL,
	"temporalidade" varchar(255) DEFAULT NULL,
	"version" integer DEFAULT NULL,
	"programa_id" integer DEFAULT NULL,
	"status" varchar(255) DEFAULT NULL,
	"usuario" bigint DEFAULT NULL,
	"parceiros" text DEFAULT NULL,
	"situacao" varchar(255) DEFAULT NULL,
	"ultima_alteracao" date DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."programas_tematicas" (
	"programa_id" integer NOT NULL,
	"tematica_id" integer NOT NULL,
	"id" serial NOT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."situacoes" (
	"id" serial NOT NULL,
	"nome" varchar(100) DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."status" (
	"id" serial NOT NULL,
	"nome" varchar(100) DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."tematicas" (
	"id" serial NOT NULL,
	"nome" varchar(255) DEFAULT NULL,
	"parent_id" integer DEFAULT NULL,
	PRIMARY KEY  ("id")
);


CREATE TABLE "public"."usuarios" (
	"id" serial NOT NULL,
	"login" varchar(255) DEFAULT NULL,
	"senha" varchar(255) NOT NULL,
	"nome" varchar(255) DEFAULT NULL,
	"ultima_alteracao" date DEFAULT NULL,
	"grupo_id" integer DEFAULT NULL,
	"email" varchar(255) DEFAULT NULL,
	"user_id" integer DEFAULT NULL,
	PRIMARY KEY  ("id")
);



