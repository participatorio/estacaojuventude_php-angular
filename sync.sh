user='humberto'
domain='estacao.sejalivre.com.br'
directory='/home/humberto/Sites/estacao/'

if [ "$1" == "remote" ]; then
	echo "Transferindo para o site remoto"
	rsync --exclude=.git --exclude=*.sql --exclude=app/tmp/* --exclude=/app/Config/database.php -aP . $user@$domain:/$directory
else 
	if [ "$1" == "local" ]; then
		echo "Transferindo para o site local"
	rsync --exclude=.git --exclude=*.sql --exclude=app/tmp/* --exclude=/app/Config/database.php -aP $user@$domain:/$directory .
	fi
	if [ "$1" == "" ]; then
		echo "sync.sh local"
		echo "sync.sh remote"
	fi
fi
