<?php
class Inicial extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 */
	public $description = 'Inicial';

/**
 * Actions to be performed
 *
 * @var array $migration
 */
	public $migration = array(
		'up' => array(
			'create_table' => array(
				'estados' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'sigla' => array('type' => 'string', 'null' => true, 'default' => null),
					'latitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'longitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'grupos' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'localizacoes' => array(
					'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'email' => array('type' => 'string', 'null' => true, 'default' => null),
					'endereco' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'horario_funcionamento' => array('type' => 'string', 'null' => true, 'default' => null),
					'local' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'nome_referencia' => array('type' => 'string', 'null' => true, 'default' => null),
					'quantidade_vagas' => array('type' => 'integer', 'null' => true, 'default' => null),
					'telefone' => array('type' => 'string', 'null' => true, 'default' => null),
					'version' => array('type' => 'integer', 'null' => true, 'default' => null),
					'ocorrencia_municipal_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'situacao' => array('type' => 'string', 'null' => true, 'default' => null),
					'status' => array('type' => 'string', 'null' => true, 'default' => null),
					'ultima_alteracao' => array('type' => 'date', 'null' => true, 'default' => null),
					'utilizador_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'status_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'situacao_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'latitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'longitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'ministerios' => array(
					'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'version' => array('type' => 'integer', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'municipios' => array(
					'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'uf' => array('type' => 'biginteger', 'null' => true, 'default' => null),
					'ultima_alteracao' => array('type' => 'date', 'null' => true, 'default' => null),
					'latitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'longitude' => array('type' => 'float', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'ocorrencia_municipal_orgaos_executores' => array(
					'ocorrencia_municipal_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'orgao_executor_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'ocorrencias_municipais' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'beneficios_locais' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'como_acessar' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'fim_inscricoes' => array('type' => 'string', 'null' => true, 'default' => null),
					'inicio_inscricoes' => array('type' => 'string', 'null' => true, 'default' => null),
					'quantidade_vagas' => array('type' => 'integer', 'null' => true, 'default' => null),
					'status' => array('type' => 'string', 'null' => true, 'default' => null),
					'municipio_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'programa_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'usuario_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'situacao' => array('type' => 'string', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'orgao_executor' => array(
					'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'contato' => array('type' => 'string', 'null' => true, 'default' => null),
					'endereco' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'site' => array('type' => 'string', 'null' => true, 'default' => null),
					'version' => array('type' => 'integer', 'null' => true, 'default' => null),
					'ultima_alteracao' => array('type' => 'date', 'null' => true, 'default' => null),
					'sigla' => array('type' => 'string', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'programa_orgaos_executores' => array(
					'programa_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'orgao_executor_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'programas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'beneficios' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'criterios_acesso' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'data_inicio' => array('type' => 'date', 'null' => true, 'default' => null),
					'descricao' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'duracao' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'idade_maxima' => array('type' => 'integer', 'null' => true, 'default' => null),
					'idade_minima' => array('type' => 'integer', 'null' => true, 'default' => null),
					'lei_criacao' => array('type' => 'string', 'null' => true, 'default' => null),
					'nivel' => array('type' => 'string', 'null' => true, 'default' => null),
					'nome_divulgacao' => array('type' => 'string', 'null' => true, 'default' => null),
					'nome_oficial' => array('type' => 'string', 'null' => true, 'default' => null),
					'objetivos' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'publico_alvo' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'sigla' => array('type' => 'string', 'null' => true, 'default' => null),
					'temporalidade' => array('type' => 'string', 'null' => true, 'default' => null),
					'version' => array('type' => 'integer', 'null' => true, 'default' => null),
					'programa_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'status' => array('type' => 'string', 'null' => true, 'default' => null),
					'usuario' => array('type' => 'biginteger', 'null' => true, 'default' => null),
					'parceiros' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'situacao' => array('type' => 'string', 'null' => true, 'default' => null),
					'ultima_alteracao' => array('type' => 'date', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'programas_tematicas' => array(
					'programa_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'tematica_id' => array('type' => 'integer', 'null' => false, 'default' => null),
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'situacoes' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'status' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'tematicas' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
				'usuarios' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'login' => array('type' => 'string', 'null' => true, 'default' => null),
					'senha' => array('type' => 'string', 'null' => false, 'default' => null),
					'nome' => array('type' => 'string', 'null' => true, 'default' => null),
					'ultima_alteracao' => array('type' => 'date', 'null' => true, 'default' => null),
					'grupo_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'email' => array('type' => 'string', 'null' => true, 'default' => null),
					'user_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'indexes' => array(
					),
					'tableParameters' => array(),
				),
				'versoes' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
					'data_hora' => array('type' => 'datetime', 'null' => true, 'default' => null),
					'tabela' => array('type' => 'string', 'null' => true, 'default' => null),
					'valores' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
					'usuario_id' => array('type' => 'integer', 'null' => true, 'default' => null),
					'indexes' => array(
						'PRIMARY' => array('unique' => true, 'column' => 'id'),
					),
					'tableParameters' => array(),
				),
			),
		),
		'down' => array(
			'drop_table' => array(
				'estados', 'grupos', 'localizacoes', 'ministerios', 'municipios', 'ocorrencia_municipal_orgaos_executores', 'ocorrencias_municipais', 'orgao_executor', 'programa_orgaos_executores', 'programas', 'programas_tematicas', 'situacoes', 'status', 'tematicas', 'usuarios', 'versoes'
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction Direction of migration process (up or down)
 * @return bool Should process continue
 */
	public function after($direction) {
		return true;
	}
}
