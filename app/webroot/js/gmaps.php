function delayed(estado) {
	geocodeAddress(
		estado+', Brazil', 
		'setMarker', 
		mapBrasil,
		'marker'+estado,
		'Estado do '+estado,
		'http://maps.google.com/mapfiles/marker.png',
		'http://maps.google.com/mapfiles/shadow50.png',
		'<a href="#" onclick="clickEstado(\''+estado+'\')" class="mapEstado" data-estado="'+estado+'">Municípios do Estado de '+estado+'</a>', 
		true
	);
}
function clickEstado(estado){
	window.continuaCarregando = false;
	for (var i = 0; i < appTimeOut.length; i++) {
		clearTimeout(appTimeOut[i])
	}
	for (var i = 0; i<markers.length; i++) {
		title = markers[i].title;
		if (title != 'Estado do '+estado) {
			markers[i].setMap(null);
		} else {
			selMarker = i;
		}
	}
	markers = [markers[selMarker]];
	//mapBrasil.setCenter(markers[0].position);
	//mapBrasil.setZoom(7);
	markers[0].setMap(null);
	markers = [];
	loadMunicipios(estado);
}
function loadMunicipios(estado) {
	$.ajax({
		'url': '/web/Programas/mapMunicipios/'+estado,
		'dataType': 'script',
		'success': function(data) {
			setTimeout(function(){
				if (markers.length > 0) {
					var fullBounds = new google.maps.LatLngBounds();
					for(var i=0;i<markers.length;i++){
						var lat=markers[i].position.lat();
						var long=markers[i].position.lng();
						var point=new google.maps.LatLng(lat,long);
						
						fullBounds.extend(point);
					}
					mapBrasil.fitBounds(fullBounds);
				}
			}, 1500);
		}
	});
	
}
$(window).ready(function(){
	window.tempo = 1000;
	window.appTimeOut = [];
	window.continuaCarregando = true;
	window.statesId = [];
	google.maps.event.addListenerOnce(mapBrasil, 'idle', function(){
	<?php foreach($Estados as $Estado) { $sigla = $Estado['Estado']['sigla']; ?>
		statesId[<?php echo $Estado['Estado']['id']; ?>] = '<?php echo $Estado['Estado']['sigla']; ?>';
		if (continuaCarregando) {
			window.appTimeOut.push(window.setTimeout(function(){
				delayed('<?php echo $sigla;?>');
			}, tempo));
			tempo+=750;
		}
	<?php } ?>
	});
	
});
