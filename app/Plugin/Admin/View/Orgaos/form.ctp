<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index', $programa_id));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body'); ?>
	
	<?php echo $this->Bootstrap->input('nome',array('label'=>'Nome','class'=>'form-control')); ?>
	<?php echo $this->Bootstrap->input('sigla',array('label'=>'Sigla','class'=>'form-control')); ?>
	<?php echo $this->Bootstrap->input('endereco',array('label'=>'Endereço','class'=>'form-control')); ?>
	<?php echo $this->Bootstrap->input('contato',array('label'=>'Contato','class'=>'form-control')); ?>
	<?php echo $this->Bootstrap->input('site',array('label'=>'Site','class'=>'form-control')); ?>

	<?php $this->end(); ?>
