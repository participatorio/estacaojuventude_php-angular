<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'add')); ?>
	<?php echo $this->Bootstrap->btnLink('Programas', array('controller'=>'Programas')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th>Programas</th>
		<th>Nome</th>
		<th>Sigla</th>
		<th>Contato</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'edit',$Model['OrgaoExecutor']['id']), array('icon'=>'pencil','title'=>'Editar','data-toggle'=>'tooltip')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'del', $Model['OrgaoExecutor']['id']), array('style'=>'danger','icon'=>'trash','title'=>'Excluir','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			</div>
		</td>
		<td><?php echo count($Model['ProgramaOrgaoExecutor']); ?></td>
		<td><?php echo $Model['OrgaoExecutor']['nome']; ?></td>
		<td><?php echo $Model['OrgaoExecutor']['sigla']; ?></td>
		<td><?php echo $Model['OrgaoExecutor']['contato']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>
