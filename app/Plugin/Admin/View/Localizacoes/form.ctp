<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index', $ocorrencia_municipal_id));
$this->end(); ?>

<?php $this->start('form-create'); ?>
	<?php echo $this->Bootstrap->create($formModel, array('type'=>'POST')); ?>
<?php $this->end(); ?>

<?php $this->start('form-body'); ?>
	<?php echo $this->Bootstrap->input('local'); ?>
	<?php echo $this->Bootstrap->input('endereco'); ?>
	<?php echo $this->Bootstrap->input('horario_funcionamento'); ?>
	<?php echo $this->Bootstrap->input('email'); ?>
	<?php echo $this->Bootstrap->input('nome_referencia'); ?>
	<?php echo $this->Bootstrap->input('situacao', array('options'=>$Situacoes)); ?>
	<?php echo $this->Bootstrap->input('status', array('options'=>$Status)); ?>

	<?php echo $this->Bootstrap->input('latitude'); ?>
	<?php echo $this->Bootstrap->input('longitude'); ?>
	
	<div class="form-group">
		<label for="LocalizacaoEndereco">Endereço</label>
		<div class="input-group">
		<input name="data[Localizacao][endereco]" class="form-control" type="text" id="LocalizacaoEndereco">
		<span class="input-group-btn">
			<button type="button" id="LocalizacaoEnderecoBtn" class="btn btn-default">Pesquisar</button>
		</span>
		</div>
	</div>
		
	<?php echo $this->Bootstrap->end(); ?>
	
	<div class="panel panel-default">
		<div class="panel-heading">
		Google Maps
		</div>
		<div id="localizacao-map" style="width:100%; height: 300px;"></div>
		
		<div class="panel-footer"></div>
	</div>
	
	
	
	<script type="text/javascript">
		var matching = {
};
		var gIcons0 = {
};

	jQuery(document).ready(function() {
		
			var initialLocation = new google.maps.LatLng(51, 11);
			var browserSupportFlag = new Boolean();
			var myOptions = {
				zoom: 15, 
				streetViewControl: false, 
				navigationControl: false, 
				mapTypeControl: false, 
				scaleControl: false, 
				scrollwheel: false, 
				keyboardShortcuts: false, 
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			// deprecated
			gMarkers0 = new Array();
			gInfoWindows0 = new Array();
			gWindowContents0 = new Array();
		
			map0 = new google.maps.Map(
				document.getElementById("localizacao-map"), 
				myOptions
			);
			
			<?php if (!empty($this->data) && is_float( $this->data['Localizacao']['latitude'] ) && is_float( $this->data['Localizacao']['longitude'])) { ?>
			position = new google.maps.LatLng(<?php echo $this->data['Localizacao']['latitude'].', '.$this->data['Localizacao']['longitude'];?>);
			<?php } else { ?>
			position = new google.maps.LatLng(-14.45, -49.70);
			<?php } ?>
			var marker1 = new google.maps.Marker({
				position: position,
				map: map0,
				icon: '/consulta/img/m1.png',
				animation: google.maps.Animation.DROP,
				draggable: true
			});
			gMarkers0 .push(
				marker1
			);
			
			google.maps.event.addListener(marker1, 'mouseup', function() {
				//map0.setCenter(x0.getPosition());
				pos = marker1.getPosition();
				$('#LocalizacaoLatitude').val(pos.k);
				$('#LocalizacaoLongitude').val(pos.B);
			});
		
			map0.setCenter(marker1.getPosition());
			
			$('#LocalizacaoEnderecoBtn').click(function(){
				carregarNoMapa($('#LocalizacaoEndereco').val());
			});
			
			function carregarNoMapa(endereco) {
				//console.log(endereco);
				geocoder = new google.maps.Geocoder;
				geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						if (results[0]) {
							var latitude = results[0].geometry.location.lat();
							var longitude = results[0].geometry.location.lng();
 
							$('#LocalizacaoEndereco').val(results[0].formatted_address);
							$('#LocalizacaoLatitude').val(latitude);
							$('#LocalizacaoLongitude').val(longitude);
 
							var location = new google.maps.LatLng(latitude, longitude);
							marker1.setPosition(location);
							map0.setCenter(location);
							map0.setZoom(15);
						}
					}
				});
			}
		

	});
</script>

<?php echo $this->end(); ?>