<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'adicionar')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th>Ocorrências</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'editar', $Model['Programa']['id']), array('icon'=>'pencil','title'=>'Editar')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('controller'=>'Tematicas','action'=>'programas', $Model['Programa']['id']), array('icon'=>'list-alt','title'=>'Temáticas')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'deletar', $Model['Programa']['id']), array('style'=>'danger','title'=>'Excluir','icon'=>'trash','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			</div>
		</td>
		<td><?php echo count($Model['OcorrenciaMunicipal']); ?></td>
		<td><?php echo $Model['Programa']['nome_oficial']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>
