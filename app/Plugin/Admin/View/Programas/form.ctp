<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
	
	echo $this->Bootstrap->input('nome_oficial');
	echo $this->Bootstrap->input('nome_divulgacao', array('label'=>'Nome Divulgação'));
	echo $this->Bootstrap->input('descricao',array('label'=>'Descrição','class'=>'form-control ckeditor'));
	echo $this->Bootstrap->input('objetivos',array('class'=>'form-control ckeditor'));
	echo $this->Bootstrap->input('beneficios',array('label'=>'Benefícios','class'=>'form-control ckeditor'));
	echo $this->Bootstrap->input('criterios_acesso',array('label'=>'Critérios de Acesso','class'=>'form-control ckeditor'));
	echo $this->Bootstrap->input('data_inicio', array('label'=>'Data de Início','type'=>'text','class'=>'form-control datepicker'));
	echo $this->Bootstrap->input('duracao', array('label'=>'Duração'));
	echo $this->Bootstrap->input('idade_minima', array('label'=>'Idade Mínima'));
	echo $this->Bootstrap->input('idade_maxima', array('label'=>'Idade Máxima'));
	echo $this->Bootstrap->input('lei_criacao', array('label'=>'Lei de Criação'));
	echo $this->Bootstrap->input('nivel', array('label'=>'Nível'));
	echo $this->Bootstrap->input('publico_alvo', array('label'=>'Público Alvo'));
	echo $this->Bootstrap->input('sigla');
	echo $this->Bootstrap->input('temporalidade');
	echo $this->Bootstrap->input('parceiros');
	echo $this->Bootstrap->input('situacao', array('label'=>'Situação'));
	
$this->end();
?>