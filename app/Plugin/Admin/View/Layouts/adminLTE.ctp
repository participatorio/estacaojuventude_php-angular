<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Estação Juventude | <?php echo $this->name; ?></title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- jQuery 2.0.2 -->
		<?php echo $this->Html->script('Admin.jquery-2.1.1.min.js'); ?>
		<?php /* <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> */ ?>
		<!-- Bootstrap -->
		<script src="/AdminLTE/js/plugins/bootstrap.min.js" type="text/javascript"></script>
		<!-- AdminLTE App -->
		<script src="/AdminLTE/js/AdminLTE/app.js" type="text/javascript"></script>

		<?php //echo $this->Html->css('Admin.bootstrap.min.css'); ?>
		<?php //echo $this->Html->css('Admin.font-awesome.min.css'); ?>
		<?php //echo $this->Html->css('Admin.ionicons.min.css'); ?>

		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />

		<!-- Theme style -->
		<?php echo $this->Html->css('AdminLTE.AdminLTE'); ?>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- CKEditor -->
		<?php echo $this->Html->script('Bootstrap.ckeditor/ckeditor'); ?>
		<!-- Google Maps -->
		<?php echo $this->Html->script($this->GoogleMapV3->apiUrl()); ?>
		<!-- Inputmask -->
		<?php echo $this->Html->script('AdminLTE.plugins/input-mask/jquery.inputmask'); ?>
		<?php echo $this->Html->script('AdminLTE.plugins/input-mask/jquery.inputmask.date.extensions'); ?>
		
		<!-- Scripts do Admin -->
		<?php echo $this->Html->script('Admin.scripts'); ?>
		
		<!-- Scripts -->
		<?php echo $this->fetch('script'); ?>
		<?php echo $this->fetch('css'); ?>
	</head>
	<?php $skin = ($this->Session->check('skin'))?($this->Session->read('skin')):('skin-black'); ?>
	<body class="<?php echo $skin;?> fixed">
		<!-- header logo: style can be found in header.less -->
		<header class="header">
			<a href="/" class="logo">
				<img src="/consulta/img/logo_ej.png">
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<div class="navbar-right">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-adjuste"></i> Temas
								<span class="label label-success">2</span>
							</a>
							<ul class="dropdown-menu">
								<li class="header">Altera a cor do Tema</li>
								<li>
									<!-- inner menu: contains the actual data -->
									<ul class="menu">
										<li><!-- start message -->
											<a href="?skin=skin-black">
												<div class="pull-left">
													<img src="http://placehold.it/25/000/fff&text=%20" class="img-circle" alt="User Image"/>
												</div>
												<h4>
													Skin Black
	
												</h4>
												<p>Tema Escuro</p>
											</a>
										</li><!-- end message -->
										<li><!-- start message -->
											<a href="?skin=skin-blue">
												<div class="pull-left">
													<img src="http://placehold.it/25/369/fff&text=%20" class="img-circle" alt="User Image"/>
												</div>
												<h4>
													Skin Blue
	
												</h4>
												<p>Tema Azul</p>
											</a>
										</li><!-- end message -->
									</ul>
								</li>
								<?php /* <li class="footer"><a href="#">See All Messages</a></li> */ ?>
							</ul>
						</li>
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-user"></i>
								<span><?php echo $user['nome']; ?><i class="caret"></i></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header bg-light-blue">
									<img src="http://www.gravatar.com/avatar/<?php echo md5( strtolower( $user['email']) );?>" class="img-circle" alt="User Image" />
									<p>
										<?php echo $user['nome'];?>
									</p>
								</li>
								<?php /*
								<!-- Menu Body -->
								<li class="user-body">
									<div class="col-xs-4 text-center">
										<a href="#">Followers</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Sales</a>
									</div>
									<div class="col-xs-4 text-center">
										<a href="#">Friends</a>
									</div>
								</li>
								*/ ?>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Perfil</a>
									</div>
									<div class="pull-right">
										<a href="/logout" class="btn btn-default btn-flat">Sair</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="left-side sidebar-offcanvas">				   
				<!-- sidebar: style can be found in sidebar.less -->
				<br>
				<section class="sidebar">
					<!-- Sidebar user panel -->
					<div class="user-panel">
						<div class="pull-left image">
							<img src="http://www.gravatar.com/avatar/<?php echo md5( strtolower( $user['email']) );?>" class="img-thumb" alt="User Image" />
						</div>
						<div class="pull-left info">
							<p>Olá, <?php $nome = split(' ', $user['nome']); echo $nome[0];?></p>

							<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						</div>
					</div>
					<?php echo $this->Element('menu-lateral'); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

			<!-- Right side column. Contains the navbar and content of the page -->
			<aside class="right-side">				  
				<!-- Content Header (Page header) -->
				<?php /*
				<section class="content-header">
					<h1>
						Blank page
						<small>Control panel</small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Blank page</li>
					</ol>
				</section>
				*/ ?>
				<!-- Main content -->
				<section class="content">
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->fetch('content'); ?>
				</section><!-- /.content -->
				
			</aside><!-- /.right-side -->
		</div><!-- ./wrapper -->
	</body>
</html>