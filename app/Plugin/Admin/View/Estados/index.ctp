<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'adicionar')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th>Nome</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'editar', $Model['Estado']['id']), array('icon'=>'pencil','title'=>'Editar','data-toggle'=>'tooltip')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'deletar', $Model['Estado']['id']), array('style'=>'danger','icon'=>'trash','title'=>'Excluir','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			</div>
		</td>
		<td><?php echo $Model['Estado']['nome']; ?></td>
		
	</tr>
<?php } ?>
<?php $this->end(); ?>
