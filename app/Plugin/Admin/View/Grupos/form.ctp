<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create'); ?>
	<?php echo $this->Bootstrap->create($formModel, array('type'=>'POST')); ?>
<?php $this->end(); ?>

<?php $this->start('form-body'); ?>
	
	<?php echo $this->Bootstrap->input('nome',array('label'=>'Nome')); ?>
	
	<?php echo $this->Bootstrap->end(); ?>
	
<?php $this->end(); ?>
