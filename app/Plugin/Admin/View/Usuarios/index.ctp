<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'adicionar')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th>Nome</th>
		<th>Login</th>
		<th>Grupo</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<?php echo $this->Element('Admin./Common/index-controls', array('id'=>$Model['Usuario']['id'])); ?>
		</td>
		<td><?php echo $Model['Usuario']['nome']; ?></td>
		<td><?php echo $Model['Usuario']['login']; ?></td>
		<td><?php echo $Model['Grupo']['nome']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>
