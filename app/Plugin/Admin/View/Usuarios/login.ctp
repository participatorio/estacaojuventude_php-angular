<div class="form-box" id="login-box">
	<div class="header">Estação Juventude</div>
		<form method="post">
			<div class="body bg-gray">
				<div class="form-group">
					<input type="text" name="data[Usuario][login]" class="form-control" placeholder="Usuario"/>
				</div>
				<div class="form-group">
					<input type="password" name="data[Usuario][senha]" class="form-control" placeholder="Senha"/>
				</div>
			</div>
			<div class="footer">
				<button type="submit" class="btn bg-olive btn-block">Entrar</button>  
				<p><a href="/admin/Usuarios/senha">Esqueci a senha</a></p>
			</div>
		</form>
	</div>
</div>
