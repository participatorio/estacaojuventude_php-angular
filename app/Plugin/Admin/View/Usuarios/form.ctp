<?php 
	$this->extend('Bootstrap./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body');
	
	//pr($this->data);
	
	echo $this->Bootstrap->input('login');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('email');

	echo $this->Bootstrap->input('grupo_id',array('options'=>$Grupos)); ?>
	
	<div class="box box-solid box-warning">
		<div class="box-header">
			<div class="box-title">Alterar Senha</div>
		</div>
		<div class="box-body">
			<?php echo $this->Bootstrap->input('password1', array('label'=>'Senha','type'=>'password')); ?>
			<?php echo $this->Bootstrap->input('password2', array('label'=>'Confirme a Senha','type'=>'password')); ?>
		</div>
	</div>
	
<?php $this->end(); ?>
