<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Bootstrap->btnLink('Adicionar', array('action'=>'adicionar')); ?>
<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
		<th>Temática Pai</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<div class="btn-group">
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'editar', $Model['Tematica']['id']), array('icon'=>'pencil')); ?>
			<?php echo $this->Bootstrap->btnLink(null, array('action'=>'delete', $Model['Tematica']['id']), array('style'=>'danger','icon'=>'trash','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
			</div>
		</td>
		<td><?php echo $Model['Tematica']['nome']; ?></td>
		<td><?php echo $Model['TopTematica']['nome']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>
