<?php
	$this->extend('Admin./Common/index'); // Extend index padrao
	$this->assign('pageHeader',$pageHeader); // Header da página
	$this->assign('subHeader', $subHeader);
	$this->assign('panelStyle',$panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>
<?php $this->start('actions');?>
	<?php echo $this->Element('Common/index-actions'); ?>
<?php $this->end(); ?>

<?php $this->start('table-header'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th class="col-md-1">Programa</th>
		<th class="col-md-2">Início/Fim</th>
		<th>Situação</th>
		<th class="col-md-4">Benefícios</th>
		<th>Município</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Model) { ?>
	<tr>
		<td>
			<?php echo $this->Element('Ocorrencias/index-controls', array('id'=>$Model['OcorrenciaMunicipal']['id'],'qtd_localizacoes'=>count($Model['Localizacao']))); ?>
		</td>
		<td><?php echo $Model['Programa']['nome_oficial']; ?></td>
		<td><?php echo $Model['OcorrenciaMunicipal']['inicio_inscricoes']; echo($Model['OcorrenciaMunicipal']['fim_inscricoes']!='')?(' a '.$Model['OcorrenciaMunicipal']['fim_inscricoes']):(''); ?></td>
		<td><span class="label label-<?php echo ($Model['Situacao']['id']==1)?('success'):('danger');?>"><?php echo $Model['Situacao']['nome']; ?></span></td>
		<td><?php echo $Model['OcorrenciaMunicipal']['beneficios_locais']; ?></td>
		<td><?php echo $Model['Municipio']['nome']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>
