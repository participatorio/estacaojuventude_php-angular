<?php 
	$this->extend('Admin./Common/form');
	$this->assign('pageHeader', $pageHeader); // Header da página
	$this->assign('subHeader', $subHeader);
	$this->assign('panelStyle', $panelStyle); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->btnLink('Gravar', array(), array('submit'=>true));
	echo $this->Bootstrap->btnLink('Cancelar', array('action'=>'index'));
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create($formModel, array('type'=>'POST'));
$this->end();
$this->start('form-body'); ?>
	<?php echo $this->Bootstrap->input('municipio_id', array('label'=>'Município', 'options'=>$Municipios)); ?>
	<?php echo $this->Bootstrap->input('programa_id', array('label'=>'Programa', 'options'=>$Programas)); ?>
	<?php echo $this->Bootstrap->input('situacao_id', array('label'=>'Situação', 'options'=>$Situacoes)); ?>
	<div class="row">
		<div class="col-md-6">
	<?php echo $this->Bootstrap->input('inicio_inscricoes', array('label'=>'Início', 'type'=>'text', 'class'=>'form-control mask-data')); ?>
		</div>
		<div class="col-md-6">	
		<?php echo $this->Bootstrap->input('fim_inscricoes', array('label'=>'Fim', 'type'=>'text', 'class'=>'form-control mask-data')); ?>
		</div>
	</div>
	<?php echo $this->Bootstrap->input('quantidade_vagas',array('label'=>'Quantidade de Vagas','class'=>'form-control')); ?>
	<?php echo $this->Bootstrap->input('beneficios_locais',array('label'=>'Benefícios','class'=>'form-control ckeditor')); ?>
	<?php echo $this->Bootstrap->input('como_acessar',array('label'=>'Como Acessar','class'=>'form-control ckeditor')); ?>
	
	<?php $this->end(); ?>
