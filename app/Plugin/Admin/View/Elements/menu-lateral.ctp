<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu">
						<li class="active">
							<?php echo $this->Html->link('<i class="fa fa-dashboard"></i></i>&nbsp Painel de Controle', array('controller'=>'Status','action'=>'index'), array('escape'=>false)); ?>
						</li>
						
						
						<li class="treeview">
							<a href="#">
								<i class="fa fa-bar-chart-o"></i>
								<span>Ocorrências</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Listar', array('controller'=>'Ocorrencias','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Adicionar', array('controller'=>'Ocorrencias','action'=>'add'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-bar-chart-o"></i>
								<span>Programas</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Listar', array('controller'=>'Programas','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Adicionar', array('controller'=>'Programas','action'=>'add'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-bar-chart-o"></i>
								<span>Órgão Executor</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Listar', array('controller'=>'Orgaos','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Adicionar', array('controller'=>'Orgaos','action'=>'add'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-bar-chart-o"></i>
								<span>Temáticas</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Listar', array('controller'=>'Tematicas','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Adicionar', array('controller'=>'Tematicas','action'=>'add'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-table"></i>
								<span>Tabelas</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Estados', array('controller'=>'Estados','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Municípios', array('controller'=>'Municipios','action'=>'index'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-bar-chart-o"></i>
								<span>Relatórios</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-angle-double-right"></i>&nbsp Principal', array('controller'=>'Relatorios','action'=>'index'), array('escape'=>false)); ?></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-user"></i>
								<span>Usuários</span>
								<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li><?php echo $this->Html->link('<i class="fa fa-user"></i>&nbsp Usuários', array('controller'=>'Usuarios','action'=>'index'), array('escape'=>false)); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-users"></i>&nbsp Grupos', array('controller'=>'Grupos','action'=>'index'), array('escape'=>false)); ?></li>
							</ul>
						</li>
					</ul>