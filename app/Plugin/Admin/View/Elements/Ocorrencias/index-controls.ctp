<div class="btn-group">
	<?php echo $this->Bootstrap->btnLink(null, array('action'=>'edit', $id), array('size'=>'btn-sm','icon'=>'pencil','title'=>'Editar','data-toggle'=>'tooltip')); ?>
	<?php echo $this->Bootstrap->btnLink('<span class="label label-success">'.$qtd_localizacoes.'</span>', array('controller'=>'Localizacoes', 'action'=>'index', $id), array('size'=>'btn-sm','icon'=>'map-marker','title'=>'Localizações')); ?>
	<?php echo $this->Bootstrap->btnLink(null, array('action'=>'del', $id), array('size'=>'btn-sm','style'=>'danger','icon'=>'trash','title'=>'Excluir','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
</div>
