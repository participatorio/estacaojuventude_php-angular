<script type="text/javascript">$(document).ready(function(){
	// Adiciona a class "active" ao menu atual
	$('ul.dropdown-menu li.active').each(function(){
		$(this).parents('.dropdown').addClass('active');
	})
});
</script>

<nav class="navbar navbar-default navbar-fixed-top hidden-print" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Alterar navigação</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/admin"><?php echo $system['name'];?></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<?php if (!empty($menus) and $user) {
					foreach ($menus as $link) { ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $link['Link']['texto'];?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<?php foreach ($link['children'] as $sublink) {
								$active = ($sublink['Permissao']['controller'] == $this->params['controller'] AND $sublink['Permissao']['action'] == $this->params['action'])?('class="active"'):('');
							?>
							<li <?php echo $active; ?>>
								<?php echo $this->Html->link($sublink['Link']['texto'], array('plugin'=>$sublink['Permissao']['plugin'],'controller'=>$sublink['Permissao']['controller'],'action'=>$sublink['Permissao']['action'])); ?>
							</li>
						<?php } ?>
						</ul>
					</li>
					<?php } ?>
				<?php } ?>
			</ul>
			<div class="btn-group navbar-right">
				<?php if (isset($formDataHere)) { ?>
				<a href="/sistema/Belongs/load" class="btn btn-warning navbar-btn">Formulário</a>
				<?php } ?>
				<a href="/admin" type="button" class="btn btn-default navbar-btn">Admin</a>
				<a href="/logout" type="button" class="btn btn-default navbar-btn">Sair
				<?php if(isset($div_timer) && $div_timer['show'] == true) { ?>&nbsp;&nbsp;<span data-timeout="<?php echo $div_timer['timeout'];?>" data-logout="<?php echo ($div_timer['logout']);?>" id="div-timer" class="label label-success" style="font-size:120%;"><?php echo ($div_timer['timeout']);?>:00</span><?php } ?>
				</a>
			</div>
		</div>
	</div>
</nav>
<?php //} ?>