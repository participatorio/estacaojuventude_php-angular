<div class="btn-group">
	<?php echo $this->Bootstrap->btnLink(null, array('action'=>'editar', $id), array('size'=>'btn-sm', 'icon'=>'pencil','title'=>'Editar','data-toggle'=>'tooltip')); ?>
	<?php echo $this->Bootstrap->btnLink(null, array('action'=>'excluir', $id), array('size'=>'btn-sm', 'style'=>'danger','icon'=>'trash','title'=>'Excluir','method'=>'post','prompt'=>true,'message'=>'Tem Certeza?')); ?>
</div>
