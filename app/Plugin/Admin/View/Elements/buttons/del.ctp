<?php 
	echo $this->Bootstrap->btnLink(
		null, 
		array(
			'action'=>'deletar', 
			$id
		), 
		array(
			'style'=>'danger',
			'title'=>'Excluir',
			'icon'=>'trash',
			'method'=>'post',
			'prompt'=>true,
			'message'=>'Tem Certeza?'
		)
	);