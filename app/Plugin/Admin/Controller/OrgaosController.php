<?php

class OrgaosController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.OrgaoExecutor');
	
	public $paginate = array(
        'limit' => 25
    );
	
	// CRUD
	public function index($programa_id = null) {
		
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('OrgaoExecutor'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Órgãos Executores');
		$this->set('panelStyle', 'primary');
		
	}
	
	public function add($programa_id = null) {
	
		$this->_save(null, $programa_id);
		
		$this->_related($programa_id);
		
		$this->set('pageHeader', 'Nova Ocorrência Municipal');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'OrgaoExecutor');
		
		$this->render('form');
	}
	
	public function edit($id = null) {
	
		$this->_save($id);
		
		$this->_related();
		
		$this->data = $this->OrgaoExecutor->read(null, $id);
		
		$this->set('pageHeader', 'Edita Ocorrência Municipal');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'OrgaoExecutor');
		
		$this->render('form');
		
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			if ($this->OrgaoExecutor->delete($id)) {
				$this->Session->setFlash('Excluído com sucesso!');
			} else {
				$this->Bootstrap->setFlash('Erro ao excluir!', 'danger');
			}
		}
		$this->_redirect();
	}
	
	private function _save($id = null, $programa_id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			// Se estiver editando
			if ($id != null) $data['OrgaoExecutor']['id'] = $id;
			else unset($data['OrgaoExecutor']['id']);
			
			$data['OrgaoExecutor']['programa_id'] = $programa_id;
			$data['OrgaoExecutor']['utilizador_id'] = $this->user['Utilizador']['id'];

			//pr($data);

			if ($this->OrgaoExecutor->save($data)) {
				$this->Bootstrap->setFlash('Salvo com successo!');
				$this->_redirect(array('action'=>'index'), $programa_id);
			}
		}
	}
	
	private function _related() {
		//$this->set('programa_id', $programa_id);
		//$Municipios = $this->OrgaoExecutor->Municipio->find('list',array('fields'=>array('id','nome')));
		//$this->set('Municipios', $Municipios);
	}
	
	private function _redirect($direction = array('action'=>'index'), $programa_id = null) {
		array_push( $direction, $programa_id);
		$this->redirect($direction);
	}
	
}