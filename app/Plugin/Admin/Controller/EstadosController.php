<?php

class EstadosController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Estado');
	
	public function index() {
		$this->set('data', $this->Estado->find('all'));
		
		//$this->set('pagination', false);
		$this->set('pageHeader', 'Estados');
		$this->set('panelStyle', 'primary');
	}
	
	public function adicionar() {
	
		$this->set('formModel', 'Estado');
		$this->set('pageHeader', 'Estados');
		$this->set('panelStyle', 'primary');
		
		$this->render('form');
		
	}
	
	public function editar($id = null) {
	
		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['Estado']['id'] = $id;
			
			$this->Estado->save($data);
			
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index'));
		}
	
		$this->set('formModel', 'Estado');
		$this->set('pageHeader', 'Estados');
		$this->set('panelStyle', 'primary');
		
		$this->data = $this->Estado->read(null, $id);
		
		$this->render('form');
		
	}
	
	public function deletar($id = null) {
		
	}
	
	public function permissoes($id = null) {
		
	}

}
