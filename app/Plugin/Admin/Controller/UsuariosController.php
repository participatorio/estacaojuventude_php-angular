<?php

class UsuariosController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Usuario');
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('senha');
	}
	
	public function index() {
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('Usuario'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Usuários');
		$this->set('panelStyle', 'primary');
	}
	
	public function adicionar() {
		if ($this->request->isPost()) {
			$data  = $this->request->data;
			
			if ($data['Usuario']['password1'] != '') {
				if ($data['Usuario']['password1'] == $data['Usuario']['password2']) {
					$data['Usuario']['senha'] = $this->Auth->password( $data['Usuario']['password1'] );
				}
			}

			$this->Usuario->save($data);
			
			$this->Bootstrap->setFlash('Salvo com sucesso!');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('formModel', 'Usuario');
		$this->set('pageHeader', 'Usuários');
		$this->set('panelStyle', 'primary');
		
		$this->_related();

		$this->render('form');
	}
	
	public function editar($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['Usuario']['id'] = $id;
			
			// Senha
			if ($data['Usuario']['password1'] != '') {
				if ($data['Usuario']['password1'] == $data['Usuario']['password2']) {
					$data['Usuario']['senha'] = $this->Auth->password( $data['Usuario']['password1'] );
				}
			}
			
			$this->Usuario->save($data);
			$this->Bootstrap->setFlash('Salvo com sucesso!');
			$this->redirect(array('action'=>'index'));
		}
		
		$this->set('formModel', 'Usuario');
		$this->set('pageHeader', 'Usuários');
		$this->set('panelStyle', 'primary');
		
		$this->_related();
		
		$this->data = $this->Usuario->read(null, $id);

		$this->render('form');
	}
	
	public function deletar($id = null) {
		if ($this->request->isPost()) {
			$this->Usuario->delete($id);
			$this->Bootstrap->setFlash('Excluido com successo!');
		} else {
			$this->Bootstrap->setFlash('Erro ao excluir!','danger');
		}
		
		$this->_redirect(array('action'=>'index'));
	}
	
	private function _related() {
		$Grupos = array('0'=>'Selecione um Grupo') + $this->Usuario->Grupo->find('list', array('fields'=>array('id','nome')));
		$this->set('Grupos', $Grupos);
	}
		
	public function login() {
	
		$this->layout = 'Admin.adminLTE-login';
		
		if ($this->request->isPost()) {
			/*$this->Usuario->Behaviors->attach('Containable');
			$this->Usuario->contain(
				'Grupo',
				'Grupo.GrupoPermissao',
				'Grupo.GrupoPermissao.Permissao'
			);
			*/
			if ($this->Auth->login()) {
				$this->Bootstrap->setFlash('Usuário autenticado com successo!');
				$this->redirect('/admin/Status/index');
			} else {
				pr ($this->Auth->password($this->data['Usuario']['senha']));
				unset($this->request->data['Usuario']['senha']);
				$this->Bootstrap->setFlash('Erro na autenticação do Usuário!','danger');
			}
		}
		
	}
	
	public function logout() {
		$this->Bootstrap->setFlash('Usuário desconectado com successo!');
		$this->Auth->logout();
		$this->redirect('/admin/Status/index');
	}
	
	public function senha() {
		
	}

}
