<?php

class ProgramasController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Programa');
	
	public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Programa.nome_oficial' => 'asc'
        )
    );
	
	// CRUD
	public function index() {
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('Programa'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Programas');
		$this->set('panelStyle', 'primary');
		
	}
	
	public function adicionar() {
	
		$this->_save($this->data);
		
		$this->_related();
		
		$this->set('pageHeader', 'Novo Programa');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Programa');
		
		$this->render('form');
	}
	
	public function editar($id = null) {
	
		$this->_save($this->data, $id);
		
		$this->_related();
		
		$this->data = $this->Programa->read(null, $id);
		
		$this->set('pageHeader', 'Editar Programa');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Programa');
		
		$this->render('form');
		
	}
	
	public function deletar($id = null) {
		if ($this->request->isPost()) {
			if ($this->Programa->delete($id)) {
				$this->Session->setFlash('Excluído com sucesso!');
			} else {
				$this->Bootstrap->setFlash('Erro ao excluir!', 'danger');
			}
		}
		$this->_redirect();
	}
	
	private function _save($data = null, $id = null) {
		if ($this->request->isPost()) {
			
			// Se estiver editando
			if ($id != null) $data['Programa']['id'] = $id;
			else unset($data['Programa']['id']);
			
			if ($this->Programa->save($data)) {
				$this->Bootstrap->setFlash('Salvo com successo!');
				$this->_redirect();
			}
		}
	}
	
	private function _related() {
		
	}
	
	private function _redirect($direction = array('action'=>'index')) {
		$this->redirect($direction);
	}
	
}