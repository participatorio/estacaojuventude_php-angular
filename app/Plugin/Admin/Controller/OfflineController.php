<?php

class OfflineController extends AdminAppController {

	public $uses = array('Consulta.Tematica','Consulta.Estado','Consulta.OcorrenciaMunicipal');
	
	public function index() {
		
	}
	
	public function download_dados() {
		
	}

	public function download_aplicacao() {
		
	}
	
	public function app() {
		$this->response->file('/home/humberto/Sites/estacao/app/webroot/EstacaoConsulta.zip', array(
        'download' => true,
        'name' => 'estacao.zip',
    ));
	}
	
	public function data($tipo = null) {
		$this->autoRender = false;
		$this->response->type('json');
		$this->Tematica->Behaviors->attach('Containable');
		$this->Tematica->contain('ProgramaTematica');
		
		$this->Estado->Behaviors->attach('Containable');
		$this->Estado->contain();
		
		$this->Estado->Municipio->Behaviors->attach('Containable');
		$this->Estado->Municipio->contain();
		
		$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
		$this->OcorrenciaMunicipal->contain('Programa','Municipio');
		
		if ($tipo == 'tematicas') {
			$Tematicas = 'tematicas='.json_encode( $this->Tematica->find('threaded',array('fields'=>array('id','nome','parent_id'),'order'=>array('Tematica.nome'=>'ASC'))) );
			$this->response->body( $Tematicas );
			$this->response->download('tematicas.js');
		}
		
		if ($tipo == 'estados') {
			$Estados = 'estados='.json_encode( $this->Estado->find('all') );
			$this->response->body( $Estados );
			$this->response->download('estados.js');
		}
		
		if ($tipo == 'municipios') {
			$Estados = 'municipios='.json_encode( $this->Estado->Municipio->find('all') );
			$this->response->body( $Estados );
			$this->response->download('municipios.js');
		}
		
		if ($tipo == 'programas') {
			$Programas = 'programas='.json_encode( $this->OcorrenciaMunicipal->find('all') );
			$this->response->body( $Programas );
			$this->response->download('programas.js');
		}
		
		
	}
	
}