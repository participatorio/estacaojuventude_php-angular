<?php

class GruposController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Grupo');
	
	public function index() {
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('Grupo'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Grupos');
		$this->set('panelStyle', 'primary');
	}
	
	public function adicionar() {
	
		if ($this->request->isPost()) {
			
			$this->Grupo->save($this->request->data);
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index'));
		}
		
		$this->set('pageHeader', 'Grupos');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Grupo');
		
		$this->render('form');
	}
	
	public function editar($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['Grupo']['id'] = $id;
			$this->Grupo->save($data);
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index'));
		}
		
		$this->data = $this->Grupo->read(null, $id);
		
		$this->set('pageHeader', 'Grupos');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Grupo');
		
		$this->render('form');

	}
	
	public function deletar($id = null) {
		if ($this->request->isPost()) {
			$this->Grupo->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com sucesso!', 'info');
		}
		$this->redirect(array('action'=>'index'));
	}
	
	public function permissoes($id = null) {
		
	}

}
