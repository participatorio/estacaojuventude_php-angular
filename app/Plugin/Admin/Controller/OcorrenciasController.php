<?php

class OcorrenciasController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.OcorrenciaMunicipal');
	
	public $paginate = array(
        'limit' => 25,
        'order' => array(
	        'OcorrenciaMunicipal.situacao_id' => 'ASC',
	        'OcorrenciaMunicipal.inicio_inscricoes' => 'DESC'
        )
    );
	
	// CRUD
	public function index() {
		
		$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
		$this->OcorrenciaMunicipal->contain(
			'Municipio',
			'Situacao',
			'Localizacao',
			'Programa'
		);
		
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('OcorrenciaMunicipal'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Ocorrências Municipais');
		$this->set('subHeader', 'Listagem' );
		$this->set('panelStyle', 'primary');
		
	}
	
	public function add() {
		
	
		$this->_save();
		
		$this->_related();
		
		$this->set('pageHeader', 'Nova Ocorrência Municipal');
		$this->set('panelStyle', 'primary');
		$this->set('subHeader', 'Adicionar' );
		$this->set('formModel', 'OcorrenciaMunicipal');
		
		$this->render('form');
	}
	
	public function edit($id = null) {
		
		$this->_save($id);
		
		$this->_related();
		
		$this->data = $this->OcorrenciaMunicipal->read(null, $id);
		
		$this->set('pageHeader', 'Edita Ocorrência Municipal');
		$this->set('panelStyle', 'primary');
		$this->set('subHeader', 'Editar' );
		$this->set('formModel', 'OcorrenciaMunicipal');
		
		$this->render('form');
		
	}
	
	public function del($id = null) {
		if ($this->request->isPost()) {
			if ($this->OcorrenciaMunicipal->delete($id)) {
				$this->Session->setFlash('Excluído com sucesso!');
			} else {
				$this->Bootstrap->setFlash('Erro ao excluir!', 'danger');
			}
		}
		$this->_redirect();
	}
	
	private function _save($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			// Se estiver editando
			if ($id != null) $data['OcorrenciaMunicipal']['id'] = $id;
			else unset($data['OcorrenciaMunicipal']['id']);
			
			$data['OcorrenciaMunicipal']['usuario_id'] = $this->user['id'];

			if ($this->OcorrenciaMunicipal->save($data)) {
				$this->Bootstrap->setFlash('Salvo com successo!');
				$this->_redirect(array('action'=>'index'));
			}
		}
	}
	
	private function _related() {
		$Municipios = $this->OcorrenciaMunicipal->Municipio->find('list',array('fields'=>array('id','nome')));
		$this->set('Municipios', $Municipios);
		$Situacoes = $this->OcorrenciaMunicipal->Situacao->find('list',array('fields'=>array('id','nome')));
		$this->set('Situacoes', $Situacoes);
		$Programas = $this->OcorrenciaMunicipal->Programa->find('list',array('fields'=>array('id','nome_oficial')));
		$this->set('Programas', $Programas);
		
	}
	
	private function _redirect($direction = array('action'=>'index')) {
		array_push( $direction );
		$this->redirect($direction);
	}
	
}