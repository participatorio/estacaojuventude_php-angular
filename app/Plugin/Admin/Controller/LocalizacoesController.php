<?php

class LocalizacoesController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Localizacao');
	
	public function index($ocorrencia_id = null) {
		
		$this->_related($ocorrencia_id);
		
		$conditions = array(
			
			'Localizacao.ocorrencia_municipal_id' => $ocorrencia_id
			
		);
				
		$this->Paginator->settings = $this->paginate;
		$Localizacoes = $this->Paginator->paginate('Localizacao', $conditions);
		$this->set('data', $Localizacoes );
			
		$this->set('pagination', true);
		$this->set('pageHeader', 'Localizações');
		$this->set('panelStyle', 'primary');
	}
	
	public function adicionar($ocorrencia_id = null) {
	
		$this->_related($ocorrencia_id);

		if ($this->request->isPost()) {
			
			$data = $this->request->data;
			$data['Localizacao']['ocorrencia_municipal_id'] = $ocorrencia_id;
			
			$this->Localizacao->save($data);
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index', $ocorrencia_id));
		}
		
		$this->set('pageHeader', 'Localizações');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Localizacao');
		
		$this->render('form');
	}
	
	public function editar($id = null, $ocorrencia_id = null) {

		$this->_related($ocorrencia_id);

		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['Localizacao']['id'] = $id;
			$this->Localizacao->save($data);
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			
			$this->redirect(array('action'=>'index', $ocorrencia_id));
		}
		
		$this->data = $this->Localizacao->read(null, $id);
		
		$this->set('pageHeader', 'Localizações');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Localizacao');
		
		$this->render('form');

	}
	
	private function _related($ocorrencia_id) {
		$this->set('ocorrencia_municipal_id', $ocorrencia_id);
		
		$Situacaoes = $this->Localizacao->Situacao->find('list', array('fields'=>array('id','nome')));
		$this->set('Situacoes', $Situacaoes);
		$Status = $this->Localizacao->Status->find('list', array('fields'=>array('id','nome')));
		$this->set('Status', $Status);
	}
	
	public function deletar($id = null, $ocorrencia_id = null) {
		if ($this->request->isPost()) {
			$this->Localizacao->delete($id);
			$this->Bootstrap->setFlash('Registro excluido com sucesso!', 'info');
		}
		$this->redirect(array('action'=>'index', $ocorrencia_id));
	}
	
	public function permissoes($id = null) {
		
	}

}
