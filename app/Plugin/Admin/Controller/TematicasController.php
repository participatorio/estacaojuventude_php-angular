<?php

class TematicasController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Tematica');
	
	public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Tematica.nome' => 'asc'
        )
    );
	
	// CRUD
	public function index() {
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('Tematica'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Temáticas');
		$this->set('panelStyle', 'primary');
		
	}
	
	public function adicionar() {
	
		$this->_save();
		
		$this->_related();
		
		$this->set('pageHeader', 'Nova Temática');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Tematica');
		
		$this->render('form');
	}
	
	public function editar($id = null) {
	
		$this->_save($id);
		
		$this->_related(array($id));
		
		$this->data = $this->Tematica->read(null, $id);
		
		$this->set('pageHeader', 'Edita Temática');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'Tematica');

		$this->render('form');
		
	}
	
	public function deletar($id = null) {
		if ($this->request->isPost()) {
			if ($this->Tematica->delete($id)) {
				$this->Session->setFlash('Excluído com sucesso!');
			} else {
				$this->Session->setFlash('Erro ao excluir!', 'danger');
			}
		}
		$this->_redirect();
	}
	
	public function programas($programa_id = null) {
		$this->set('pageHeader', 'Temáticas do Programa');
		$this->set('panelStyle', 'primary');
		
		$this->Tematica->ProgramaTematica->Behaviors->attach('Containable');
		$this->Tematica->ProgramaTematica->contain(
			'Tematica',
			'Tematica.TopTematica',
			'Programa'
		);
		
		$conditions = array(
			'ProgramaTematica.programa_id' => $programa_id
		);
		$ProgramaTematicas = $this->Tematica->ProgramaTematica->find('all', array('conditions'=>$conditions));
		$this->set('data', $ProgramaTematicas);
		
		$Programa = $this->Tematica->ProgramaTematica->Programa->read(null, $programa_id);
		$this->set('Programa', $Programa);
		
		$this->set('programa_id', $programa_id);
	}
	
	public function adicionar_programa($programa_id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['ProgramaTematica']['programa_id'] = $programa_id;
			$this->Tematica->ProgramaTematica->save($data);
			
			$this->Bootstrap->setFlash('Adicionado com successo!');
			$this->_redirect(array('action'=>'programas', $programa_id));
				
		}
		$this->set('pageHeader', 'Adicionar Temática ao Programa');
		$this->set('panelStyle', 'primary');
		$this->set('formModel', 'ProgramaTematica');
		
		$this->set('Tematicas', $this->Tematica->find('list',array('fields'=>array('id','nome'))));

		$this->render('form_programa');
		
	}
	
	public function deletar_programa($id = null, $programa_id = null) {
		if ($this->request->isPost()) {
			$this->Tematica->ProgramaTematica->delete($id);
			$this->Bootstrap->setFlash('Temática excluidada do Programa com sucesso!');
			$this->_redirect(array('action'=>'programas', $programa_id));
		} else {
			$this->Bootstrap->setFlash('Não foi possível excluir!');
			$this->_redirect(array('action'=>'programas', $programa_id));
		}
	}
	
	private function _save($id = null) {
		if ($this->request->isPost()) {
			$data = $this->request->data;
			// Se estiver editando
			if ($id != null) $data['Tematica']['id'] = $id;
			if ($data['Tematica']['parent_id'] == 0) {
				$data['Tematica']['parent_id'] = null;
			}
			
			
			if ($this->Tematica->save($data)) {
				$this->Bootstrap->setFlash('Salvo com successo!');
				$this->_redirect(array('index'));
			}
			
		}
	}
	
	private function _related($exclude = array()) {
		$conditions = array(
			'Tematica.parent_id iS NULL',
			'NOT' => array(
				'id' => $exclude
			)
		);
		$order = array(
			'Tematica.nome'=>'ASC'
		);
		$Tematicas = array('0'=>'Nenhum') + $this->Tematica->find('list', array('fields'=>array('id','nome'),'conditions'=>$conditions,'order'=>$order));
		$this->set('Tematicas', $Tematicas);
	}
	
	private function _redirect($direction = array('action'=>'index')) {
		$this->redirect($direction);	
	}
	
}