<?php

class MunicipiosController extends AdminAppController {

	// Usa model do plugin Web ( Consulta Web )
	public $uses = array('Consulta.Municipio');
	
	public function index() {
		$this->Paginator->settings = $this->paginate;
		$this->set('data', $this->Paginator->paginate('Municipio'));
		
		$this->set('pagination', true);
		$this->set('pageHeader', 'Municípios');
		$this->set('panelStyle', 'primary');
	}
	
	public function adicionar() {
	
		if ($this->request->isPost()) {
			$data = $this->request->data;
			
			$this->Municipio->save($data);
			
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index'));
		}
		
		$this->set('formModel', 'Municipio');
		$this->set('pageHeader', 'Municípios');
		$this->set('panelStyle', 'primary');
		
		$this->_related();
		
		$this->render('form');
	}
	
	public function editar($id = null) {
		
		if ($this->request->isPost()) {
			$data = $this->request->data;
			$data['Municipio']['id'] = $id;
			
			$this->Municipio->save($data);
			
			$this->Bootstrap->setFlash('Registro salvo com sucesso!', 'info');
			$this->redirect(array('action'=>'index'));
		}
	
		$this->set('formModel', 'Municipio');
		$this->set('pageHeader', 'Municípios');
		$this->set('panelStyle', 'primary');
		
		$this->_related();
		
		$this->Municipio->Behaviors->attach('Containable');
		$this->Municipio->contain();

		$this->data = $this->Municipio->read(null, $id);
		
		$this->render('form');
		
	}
	
	public function deletar($id = null) {
		
	}
	
	public function permissoes($id = null) {
		
	}
	
	private function _related() {
		$this->set('Estados', $this->Municipio->Estado->find('list', array('fields'=>array('id','nome'))));
	}

}
