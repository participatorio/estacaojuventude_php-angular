<?php

App::uses('Controller', 'Controller');

class AdminAppController extends AppController {
	
	public $uses = array('Admin.Versao','Consulta.Usuario');
	public $helpers = array(
		'Tools.GoogleMapV3',
		'Session'
	);
	
	public $components = array(
		'Bootstrap.Bootstrap',
		'Session',
		'Auth' => array(
			'authenticate' => array(
				'Form' => array(
					'fields' => array(
						'username' => 'login',
						'password' => 'senha'
					),
					'userModel' => 'Usuario'
				),
			),
			'loginAction' => array(
				'plugin' => 'admin',
				'controller' => 'Usuarios',
				'action' => 'login'
			)
		)
	);
	
	public function beforeFilter() {
		
		parent::beforeFilter();
		
		// Carregar Layout bootstrap
		$this->layout = 'Admin.adminLTE';
		$user = $this->Auth->user();
		$this->user = $user;
		$this->set('user', $user);
		
		// Skin
		if (isset($this->params->query['skin'])) {
			$this->Session->write('skin', $this->params->query['skin']);
		}
		
		$this->system = Configure::read('system');
		// Configuração do Timer de logout automático
		$this->set('div_timer', array(
			'show' => true,
			'logout' => '/logout',
			'timeout' => 21
		));
		
	}

}
