<div class="row">
	<div class="col-md-4"><h3><?php echo ($this->fetch('pageHeader'))?($this->fetch('pageHeader')):(''); ?></h3></div>
	<div class="col-md-8 clearfix">
		<div class="pull-right">
			<?php echo ($this->fetch('actions'))?($this->fetch('actions')):('');?>
		</div>
	</div>
</div>
<hr>
<?php /*
<div class="row">
	<div class="col-md-12">
		<form method="post" class="form-inline" role="form">
			<div class="form-group">
				<label class="sr-only" for="search">Texto</label>
				<input value="<?php echo (isset($search))?($search):('');?>" size="50" name="data[search]" type="text" class="form-control" id="search" placeholder="Texto para pesquisa">
			</div>
			<button type="submit" class="btn btn-default">Pesquisar...</button>
		</form>
	</div>
</div>
*/ ?>
<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-12">
		<?php echo $this->fetch('table-body'); ?>
	</div>
</div>
