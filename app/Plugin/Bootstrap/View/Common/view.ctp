<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>

<?php echo $this->fetch('bread'); ?>
<h3><?php echo $this->fetch('pageHeader'); ?></h3>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-<?php echo $panelStyle;?>">
			<div class="panel-heading"><h3 class="panel-title">Detalhes do Programa</h3></div>
			<div class="panel-body">
				<?php echo $this->fetch('body'); ?>
			</div>
			<div class="panel-footer">
				&nbsp;
				<span class="pull-right"><g:plusone data-size="medium" data-annotation="none"></g:plusone></span>
				<span class="pull-right">
					<fb:like href="https://developers.facebook.com/docs/plugins/" layout="button" action="like" show_faces="false" share="false"></fb:like>
					&nbsp;
				</span>
			</div>
		</div>
		
	</div>
</div>
