<?php echo $this->fetch('bread'); ?>
<h3><?php echo ($this->fetch('pageHeader'))?($this->fetch('pageHeader')):('Crie o Block pageHeader'); ?></h3>
<hr>
<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-4">
		<?php echo $this->fetch('temas'); ?>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<select class="form-control" id="pesquisar-estado">
						<option value="0">Todos os Estados</option>
						<?php foreach($Estados as $Estado) { ?>
							<option value="<?php echo $Estado['Estado']['id'];?>"><?php echo $Estado['Estado']['nome']; ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<select class="form-control" id="pesquisar-municipio" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">
						<option value="0">Todos os Municípios do Estado</option>
					</select>
				</div>
			</div>
			<div class="col-md-2">
				<button class="btn btn-default" id="btn-pesquisar-programas" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">Pesquisar</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" id="programas-container"></div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.btn-tema-programa').click(function(){
			if($(this).data('selected') == '1') {
				$(this).find('.glyphicon').removeClass('glyphicon-ok-circle').addClass('glyphicon-remove-circle');
				$(this).data('selected', 0);
			} else {
				$(this).find('.glyphicon').removeClass('glyphicon-remove-circle').addClass('glyphicon-ok-circle');
				$(this).data('selected', 1);
			}
		});
		$('#btn-pesquisar-programas').click(function(){
			$('#programas-container').html('');
			$('#btn-pesquisar-programas').popover('show');
			temas = '0';
			$('span.glyphicon-ok-circle').each(function(){
				temas += ','+$(this).data('value');
			});
			$.ajax({
				'url': '/programas/pesquisar/page:5',
				'type': 'post',
				'data': {'temas':temas,'estado':$('#pesquisar-estado').val(),'municipio':$('#pesquisar-municipio').val()},
				'success': function(data) {
					$('#btn-pesquisar-programas').popover('hide');
					$('#programas-container').html(data);
				}
			});
		});
		
		$('#pesquisar-estado').change(function() {
			$('#pesquisar-municipio').popover('show');
			$.ajax({
				'url': '/programas/municipios/'+$(this).val(),
				'type':'post',
				'success': function(data) {
					$('#pesquisar-municipio').popover('hide');
					$('#pesquisar-municipio').html(data);
				}
			})
		});
		
	});
</script>
