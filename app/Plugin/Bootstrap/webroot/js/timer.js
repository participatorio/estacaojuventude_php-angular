$(document).ready(function(){
	timer_minutes = 1;//parseInt( $('#div-timer').data('timeout') );
	timer_seconds = 0;
	timer_logout = $('#div-timer').data('logout');
	
	timer_interval = setInterval(function(){
		timer_seconds--;
		if (timer_seconds < 0) {
			timer_seconds = 59;
			timer_minutes--;
		}
		$('#div-timer').html(timer_minutes+':'+timer_seconds);
		if (timer_minutes < 0) {
			clearInterval(timer_interval);
			location.href = timer_logout;
		}
	}, 1000);

});