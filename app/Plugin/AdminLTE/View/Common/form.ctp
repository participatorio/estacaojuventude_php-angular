<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<?php echo $this->fetch('form-create'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="page-header no-margin clearfix">
			<div class="text pull-left">
				<h2><?php echo $this->fetch('pageHeader');?></h2>
				<small><?php echo $this->fetch('pageSubHeader');?></small>
			</div>
		</div>
		<div class="box box-<?php echo $panelStyle;?>">
			<div class="box-body">
				<?php echo $this->fetch('form-body'); ?>
			</div>
		</div>
	</div>
</div>
<?php echo $this->Bootstrap->end(); ?>
<?php echo $this->fetch('scripts'); ?>
