<div class="user-panel">
	<div class="pull-left image">
		<input value="21" type="text" data-width="40" data-height="40" class="sessTime dial">
	</div>
	<div class="pull-left info">
		<p><?php echo __d('admin_lte','Hello');?>, <?php echo $usuario['nome'];?></p>
		<p><a href="#"><i class="fa fa-circle text-success"></i> <?php echo __d('admin_lte','Online');?></a></p>
	</div>
</div>
<div class="user-panel" style="color:#fff;">
	<h4>Ações<br><small><?php echo $this->fetch('pageHeader'); ?></small></h4>
	<?php echo $this->fetch('actions'); ?>
</div>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li <?php echo($this->name == 'Status')?('class="active"'):('');?> >
		<a href="/">
			<i class="fa fa-dashboard"></i> <span><?php echo __d('admin_lte','Status');?></span>
		</a>
	</li>
	<?php foreach($menus as $menu) { ?>
	<?php $found = false; ?>
	<?php foreach($menu['children'] as $submenu) {
		if ($this->name == $submenu['Permissao']['controller']) $found = true;
	} ?>
	<li class="treeview <?php echo ($found)?('active'):('');?>">
		<a href="#">
			<i class="fa fa-table"></i>
			<span><?php echo $menu['Link']['texto'];?></span>
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu" style="<?php echo ($found)?('display: block;'):('');?>">
			<?php foreach($menu['children'] as $submenu) {
			$active = ($this->name == $submenu['Permissao']['controller'])?('active'):('');
			?>
			<li class="<?php echo $active;?>"><a href="/<?php echo $submenu['Permissao']['plugin'];?>/<?php echo $submenu['Permissao']['controller'];?>/<?php echo $submenu['Permissao']['action'];?>"><i class="fa fa-angle-double-right"></i>&nbsp;<?php echo $submenu['Link']['texto'];?></a></li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>
	<li>
		<a href="#top">
			<i class="fa fa-arrow-up"></i> <span><?php echo __d('admin_lte','Top');?></span>
		</a>
	</li>
</ul>
