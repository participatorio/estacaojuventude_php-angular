<?php
class ProgramasController extends ConsultaAppController {

	var $uses = array('Consulta.Programa','Consulta.Estado');
	var $helpers = array('Bootstrap.Bootstrap','Js','Paginator');
	var $components = array('RequestHandler');
	
	
	public function index() {
		
		$this->Programa->ProgramaTematica->Tematica->Behaviors->attach('Containable');
		$this->Programa->ProgramaTematica->Tematica->contain(
			'ProgramaTematica',
			'ProgramaTematica.Programa'
		);
		
		$Tematicas = $this->Programa->ProgramaTematica->Tematica->find('all', 
			array(
				'order'=>array('Tematica.nome'=>'ASC')
			)
		);
		$this->set('Tematicas', $Tematicas);
		
		$Estados = $this->Estado->find('all');
		$this->set('Estados', $Estados);
		
		if ($this->Session->check('pesquisa.estado_id')) {
			$this->set('SessionEstadoId', $this->Session->read('pesquisa.estado_id'));
			$conditions = array(
				'Municipio.uf' => $this->Session->read('pesquisa.estado_id')
			);
			$Municipios = $this->Estado->Municipio->find('all', array(
				'conditions' => $conditions
			));
			$this->set('Municipios',$Municipios);
		} else {
			$this->set('SessionEstadoId', 0);
			$this->set('Municipios', array());
		}
		
		if ($this->Session->check('pesquisa.municipio_id')) {
			$this->set('SessionMunicipioId', $this->Session->read('pesquisa.municipio_id'));
		} else {
			$this->set('SessionMunicipioId', 0);
		}
	}
	public function addMarker($estado_id = null) {
		$this->layout = 'ajax';
		$this->set('estado_sigla', $estado_id);
		
	}
	public function saveLatLngEstado() {
		$data = $this->data;
		$Estado = array(
			'id' => $data['id'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude']
		);
		$this->Estado->save($Estado);
		echo 'Estado '.$data['id'].' salvo com lat:'.$data['latitude'].' e longitude: '.$data['longitude'];
		$this->layout = 'ajax';
	}
	
	public function saveLatLngMunicipio() {
		$data = $this->data;
		$Municipio = array(
			'id' => $data['id'],
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude']
		);
		$this->Estado->save($Municipio);
		echo 'Municipio '.$data['id'].' salvo com lat:'.$data['latitude'].' e longitude: '.$data['longitude'];
		$this->layout = 'ajax';
	}
	
	public function pesquisar() {
		$conditions = array('Programa.status'=>'Efetivo');
		if (isset($this->data['municipio'])) {
			$this->Session->write('pesquisa.municipio_id', $this->data['municipio']);
		}
		if (isset($this->data['temas']) && $this->data['temas'] != '0') {
			$temas = split(',', $this->data['temas']);
			$conditions['ProgramaTematica.tematica_id IN'] = $temas;
		}
		if (isset($this->data['search'])) {
			$conditions['OR'] = array(
				array('Programa.descricao like' => '%'.$this->data['search'].'%'),
				array('Programa.beneficios like' => '%'.$this->data['search'].'%'),
				array('Programa.criterios_acesso like' => '%'.$this->data['search'].'%'),
				array('Programa.nome_divulgacao like' => '%'.$this->data['search'].'%'),
				array('Programa.nome_oficial like' => '%'.$this->data['search'].'%'),
				array('Programa.publico_alvo like' => '%'.$this->data['search'].'%'),
				array('Programa.objetivos like' => '%'.$this->data['search'].'%')
			);
		}
		//pr($conditions);
		if (isset($this->data['estado']) && $this->data['estado'] != '0') {
			$OcorCond = array();
			$OcorCond['Municipio.uf'] = $this->data['estado'];
			if (isset($this->data['search'])) {
				$OcorCond['Municipio.nome like'] = '%'.$this->data['search'].'%';
			}
			$Ocorrencias = $this->Programa->OcorrenciaMunicipal->find(
				'all',
				array(
					'fields' => array('programa_id'),
					'conditions' => $OcorCond
				)
			);
			$ProgramasEstado = array();
			foreach($Ocorrencias as $Ocorrencia) {
				array_push($ProgramasEstado, $Ocorrencia['OcorrenciaMunicipal']['programa_id']);
			}
			$conditions['Programa.id'] = $ProgramasEstado;
		}
		
		if (isset($this->data['municipio']) && $this->data['municipio'] != '0') {
			
			$Ocorrencias = $this->Programa->OcorrenciaMunicipal->find(
				'all',
				array(
					'fields' => array('programa_id'),
					'conditions' => array(
						'Municipio.id' => $this->data['municipio']
					)
				)
			);
			$ProgramasMunicipio = array();
			foreach($Ocorrencias as $Ocorrencia) {
				array_push($ProgramasMunicipio, $Ocorrencia['OcorrenciaMunicipal']['programa_id']);
			}
			$conditions['Programa.id'] = $ProgramasMunicipio;
		}
		$Ocorrencias = $this->Programa->OcorrenciaMunicipal->find('all');
		$ProgramasComOcorrencia = array();
		foreach($Ocorrencias as $Ocorrencia) {
			array_push($ProgramasComOcorrencia, $Ocorrencia['OcorrenciaMunicipal']['programa_id']);
		}
		$conditions['Programa.id'] = $ProgramasComOcorrencia;
		
		//pr($conditions);
		
		$this->paginate = array(
			'limit' => 5,
			'contain' => array(
				'Programa',
				'Tematica',
				'Programa.OcorrenciaMunicipal',
				'Programa.OcorrenciaMunicipal.Municipio'
			),
			'conditions' => $conditions
		);
		$this->Programa->ProgramaTematica->Behaviors->attach('Containable');
		$data = $this->Paginator->paginate('Programa.ProgramaTematica');
		$this->set('data', $data);
		$this->layout = 'ajax';
	}
	
	public function municipios($estado_id = null) {
		$conditions = array(
			'Municipio.uf' => $estado_id
		);
		$Municipios = $this->Estado->Municipio->find('all', array(
			'conditions' => $conditions
		));
		//pr($Municipios);
		$this->Session->write('pesquisa.estado_id', $estado_id);
		$this->set('Municipios',$Municipios);
		$this->layout = 'ajax';
	}
	public function mapMunicipios($estado_sigla = null) {
		$Estado = $this->Estado->find('first', array(
			'conditions' => array(
				'Estado.sigla' => $estado_sigla
			)
		));
		$conditions = array(
			'Municipio.uf' => $Estado['Estado']['id']
		);
		$this->Estado->Municipio->Behaviors->attach('Containable');
		$this->Estado->Municipio->contain(
			'OcorrenciaMunicipal',
			'OcorrenciaMunicipal.Programa',
			'OcorrenciaMunicipal.Localizacao'
		);
		$Municipios = $this->Estado->Municipio->find('all', array(
			'conditions' => $conditions
		));
		$this->Session->write('pesquisa.estado_id', $Estado['Estado']['id']);
		$this->set('Municipios',$Municipios);
		$this->layout = 'ajax';
	}
	
	public function view($programa_id = null) {
		$this->Programa->Behaviors->attach('Containable');
		$this->Programa->contain(
			'ProgramaVinculado',
			'OcorrenciaMunicipal',
			'OcorrenciaMunicipal.Municipio',
			'OcorrenciaMunicipal.Municipio.Estado'
			//'OcorrenciaMunicipal.Localizacao'
		);
		
		$Programa = $this->Programa->read(null, $programa_id);
		$this->set('Programa', $Programa);
		$this->set('title_for_layout', $Programa['Programa']['nome_oficial']);
	}
}