<?php
class OfflineController extends ConsultaAppController {
	
	public $uses = array('Consulta.Tematica','Consulta.Estado','Consulta.Municipio');
	
	public function index() {
		
		$this->set('Tematicas', $this->Tematica->find('all'));
		$this->set('Estados', $this->Estado->find('all'));
		$this->set('Cidades', $this->Municipio->find('all'));
	}
	
}