<?php
class ProgramaController extends ConsultaAppController {

	public $uses = array('Consulta.Programa','Consulta.Estado');
	public $helpers = array('Tools.GoogleMapV3');
	
	public function index() {
	
		$this->set('title_for_layout', 'Programas');
		$this->set('panelStyle', 'info');
	
		$this->_related();
		$this->_gmaps();
		
		if ($this->request->isPost()) {
		
			$data = $this->request->data;
			
			$this->set('isPrograma', true);
			
			if ($data['Programa']['municipio_id'] != 0) {
				$ProgramaOcorrencias = $this->Programa->OcorrenciaMunicipal->find('list',
					array(
						'fields' => array('programa_id'),
						'conditions' => array(
							'OcorrenciaMunicipal.municipio_id' => $data['Programa']['municipio_id']
						)
					)
				);
			} else $ProgramaOcorrencias = array();
			
			if ($data['Programa']['tema_id']) {
				$conditions = array(
					'ProgramaTematica.tematica_id' => explode(',', $data['Programa']['tema_id'])
				);
				if (!empty($ProgramaOcorrencias)) {
					$conditions['ProgramaTematica.programa_id'] = $ProgramaOcorrencias;
				}
				$ProgramaTematicas = $this->Programa->ProgramaTematica->find('list',
					array(
						'fields'=>array('programa_id'),
						'conditions' => $conditions
					)
				);
			} else $ProgramaTematicas = array();
			
			$ProgramasId = $ProgramaTematicas;
			
			$ProgramaConditions = array();
			
			if ($data['Programa']['tema_id'] != '') {
				$ProgramaConditions['Programa.id'] = $ProgramasId;
			}
			
			if ($data['Programa']['municipio_id'] != 0) {
				$ProgramaConditions['OcorrenciaMunicipal.municipio_id'] = $data['Programa']['municipio_id'];
			}
			
			$this->Programa->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->Programa->OcorrenciaMunicipal->contain(
				'Programa',
				'Localizacao'
			);
			
			$Programas = $this->Paginator->paginate('OcorrenciaMunicipal',$ProgramaConditions);


			$this->set('Programas', $Programas );
			$this->set('paginator', true);
			
		} else {
			$this->set('isLocalizacao', true);
		}
		
	}
	
	public function estado($estado_id) {
		$this->_related();
		$this->_gmaps('programas', array());
	}
	
	public function view($id) {
		$this->Programa->OcorrenciaMunicipal->Behaviors->attach('Containable');
		$this->Programa->OcorrenciaMunicipal->contain(
			'Programa',
			'Programa.ProgramaVinculado',
			'Localizacao'
		);
		$Ocorrencia = $this->Programa->OcorrenciaMunicipal->read(null, $id);
		$this->set('Ocorrencia', $Ocorrencia);
	}
	
	private function _gmaps($type = 'estados', $Programas = null) {
		if ($type == 'estados') { 
			$this->Estado->Behaviors->attach('Containable');
			$this->Estado->contain(
				'Municipio',
				'Municipio.OcorrenciaMunicipal',
				'Municipio.OcorrenciaMunicipal.Localizacao',
				'Municipio.OcorrenciaMunicipal.Programa'
			);
			$Estados = $this->Estado->find('all', array('fields'=>array('id','nome','sigla','latitude','longitude')));
			$this->set('gMapsData', $Estados);
			//pr($Estados);
		}
		if ($type == 'programas') {
			$this->set('gMapsData', $Programas);
		}
	}
	
	private function _related() {
		$this->Programa->ProgramaTematica->Tematica->Behaviors->attach('Containable');
		$this->Programa->ProgramaTematica->Tematica->contain('ProgramaTematica');
		$Tematicas = $this->Programa->ProgramaTematica->Tematica->find('threaded', array('fields'=>array('id','nome','parent_id'),'order'=>array('Tematica.nome'=>'ASC')));
		//pr($Tematicas);
		$this->set('Tematicas', $Tematicas);
		
		$this->Estado->Behaviors->attach('Containable');
		$this->Estado->contain();
		$Estados = $this->Estado->find('list', array('fields'=>array('id','nome')));
		$this->set('Estados', $Estados);
		
	}
	
	public function gm_municipios($estado_sigla = null) {
		$this->layout = 'ajax';
		$conditions = array(
			'Estado.sigla' => strtoupper( $estado_sigla )
		);
		$Estado = $this->Estado->find('first', array('conditions'=>$conditions));
		$this->set('Estado', $Estado);
	}
	
	public function municipios($estado_id = null) {
		$conditions = array(
			'Municipio.estado_id' => $estado_id
		);
		$Municipios = $this->Estado->Municipio->find('all', array(
			'conditions' => $conditions
		));
		//pr($Municipios);
		$this->Session->write('pesquisa.estado_id', $estado_id);
		$this->set('Municipios',$Municipios);
		$this->layout = 'ajax';
	}
	
}