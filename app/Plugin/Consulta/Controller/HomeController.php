<?php
	class HomeController extends ConsultaAppController {

		public $uses = array(
			'Consulta.Tematica',
			'Consulta.OcorrenciaMunicipal',
			'Consulta.Estado'
		);

		public $helpers = array('Tools.GoogleMapV3');

		public function index() {

			// Começa com Localização se não for POST
			if (!$this->request->isPost()) {
				$this->set('isLocalizacao', true);
			}

			// Tematicas
			$this->Tematica->ProgramaTematica->Behaviors->attach('Containable');
			$this->Tematica->ProgramaTematica->contain(
				'Tematica'
			);
			$ProgramaTematicas = $this->Tematica->ProgramaTematica->find(
				'all',
				array(
					'order' => array(
						'Tematica.nome' => 'ASC'
					)
				)
			);
			
			$Tematicas = array();
			foreach ($ProgramaTematicas as $Tematica) {
				$Tematicas[$Tematica['Tematica']['id']] = $Tematica['Tematica']['nome'];
			}
			
			$this->set('Tematicas', $Tematicas);
			
			$this->set('Estados', $this->Estado->find(
				'list',
				array(
					'fields' => array(
						'id',
						'nome'
					)
				)
			));
		}
		
		public function loadUfs() {
			
			$this->layout = 'ajax';
			
			$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->contain(
				'Programa',
				'Municipio',
				'Municipio.Estado'
			);
			$conditions = array(
				//'Programa.id' => 158 // Programa Estacao Juventude
				'OcorrenciaMunicipal.situacao_id' => '1'
			);
			
			$OcorrenciasMunicipais = $this->OcorrenciaMunicipal->find('all',array('conditions'=>$conditions));
						
			$EstadosPrograma = array();
			$EstadosEncontrados = array();
			$countMunicipios = 1;
			
			foreach($OcorrenciasMunicipais as $Ocorrencia) {
				if (isset($Ocorrencia['Municipio']['Estado'])) {
				if (!in_array($Ocorrencia['Municipio']['estado_id'], $EstadosEncontrados)) {
					array_push($EstadosPrograma, $Ocorrencia['Municipio']['Estado']);
					array_push($EstadosEncontrados, $Ocorrencia['Municipio']['estado_id']);
					$EstadosPrograma[count($EstadosPrograma)-1]['qtd_municipios'] = $countMunicipios;
				}
				}
			}
			
			$this->set('Estados', $EstadosPrograma);
			
		}
		
		public function loadMus($estado_id = null) {
						
			$this->layout = 'ajax';
			
			$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->contain(
				'Programa',
				'Municipio',
				'Municipio.Estado'
			);
			$conditions = array(
				//'Programa.id' => 158,
				'Municipio.estado_id' => $estado_id,
				'OcorrenciaMunicipal.situacao_id' => 1
			);
			
			$OcorrenciasMunicipais = $this->OcorrenciaMunicipal->find('all',array('conditions'=>$conditions));
			
			$MunicipiosPrograma = array();
			$MunicipiosEncontrados = array();
			
			foreach($OcorrenciasMunicipais as $Ocorrencia) {
				if (!in_array($Ocorrencia['Municipio']['id'], $MunicipiosEncontrados)) {
					array_push($MunicipiosPrograma, $Ocorrencia['Municipio']);
					array_push($MunicipiosEncontrados, $Ocorrencia['Municipio']['id']);
				}
			}
			
			$this->set('Municipios', $MunicipiosPrograma);
			
		}
		
		public function loadOcs($estado_sigla = '', $municipio_id = null) {
			
			$this->layout = 'ajax';
			
			$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->contain(
				'Programa',
				'Municipio',
				'Municipio.Estado',
				'Localizacao'
			);
			if ($municipio_id != 'All') {
				$conditions = array(
					'OcorrenciaMunicipal.municipio_id' => $municipio_id,
					'OcorrenciaMunicipal.situacao_id' => 1
				);
			} else {
				$conditions = array(
					'OcorrenciaMunicipal.situacao_id' => 1
				);
			}
			$OcorrenciasMunicipais = $this->OcorrenciaMunicipal->find('all',array('conditions'=>$conditions));
			$this->set('Ocorrencias', $OcorrenciasMunicipais);
			
		}
		
		public function loadTem($tematica_id = null) {
			
			$this->layout = 'ajax';
			
			$conditions = array(
				'ProgramaTematica.tematica_id' => $tematica_id
			);
			$this->OcorrenciaMunicipal->Programa->ProgramaTematica->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->Programa->ProgramaTematica->contain();
			$Programas = $this->OcorrenciaMunicipal->Programa->ProgramaTematica->find('list', array('fields'=>array('programa_id'),'conditions'=>$conditions));
			
			$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->contain(
				'Programa',
				'Municipio',
				'Municipio.Estado',
				'Localizacao'
			);
			
			$conditions = array(
				'OcorrenciaMunicipal.programa_id' => $Programas,
				'OcorrenciaMunicipal.situacao_id' => 1
			);
			
			$OcorrenciasMunicipais = $this->OcorrenciaMunicipal->find('all',array('conditions'=>$conditions));
			$this->set('Ocorrencias', $OcorrenciasMunicipais);
			$this->render('load_ocs');
			
		}
		
		private function _toKm($grades) {
			return $grades * 111.11 * -1;
		}
		private function _fromKm($kms) {
			return $kms / 111.11 * -1;
		}
		
		public function loadRadius() {
			
			$this->layout = 'ajax';
			
			$data = $this->request->data;
			
			// Calculo da distancia entre o usuario e os programas
			//pr($data);
			/*
			** Encontrar valores de lat e lng maiores e menores do que o radius
			*/
			
			// Em Km
			$latKm = $this->_toKm( $data['lat'] );
			$lngKm = $this->_toKm( $data['lng'] );
			$radKm = $data['radius'];
			
			// Latitude
			$latMenor = $latKm - $radKm;
			$latMaior = $latKm + $radKm;
			// Longitude
			$lngMenor = $lngKm - $radKm;
			$lngMaior = $lngKm + $radKm;
			
			// Condição lat/lng dentro do quadrado de radius km de lado
			$conditions = array(
				'Localizacao.latitude <' => $this->_fromKm( $latMenor ),
				'Localizacao.latitude >' => $this->_fromKm( $latMaior ),
				'Localizacao.longitude <' => $this->_fromKm( $lngMenor ),
				'Localizacao.longitude >' => $this->_fromKm( $lngMaior ) 
			);
			
			$this->OcorrenciaMunicipal->Localizacao->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->Localizacao->contain();
			$Localizacoes = $this->OcorrenciaMunicipal->Localizacao->find('list',array('fields'=>array('id'),'conditions'=>$conditions));
			
			$conditions = array(
				'OcorrenciaMunicipal.id' => $Localizacoes
			);

			$this->OcorrenciaMunicipal->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->contain(
				'Programa',
				'Municipio',
				'Municipio.Estado',
				'Localizacao'
			);
						
			$OcorrenciasMunicipais = $this->OcorrenciaMunicipal->find('all',array('conditions'=>$conditions));
			$this->set('Ocorrencias', $OcorrenciasMunicipais);
			
		}


	}