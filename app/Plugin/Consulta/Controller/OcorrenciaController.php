<?php
	class OcorrenciaController extends ConsultaAppController {
		
		public $uses = array('Consulta.OcorrenciaMunicipal','Consulta.Tematica','Consulta.Estado');
		public $helpers = array('Tools.GoogleMapV3');
		
		public function index() {
			
			$this->_related();
			$this->_gmaps();
			
		}
		
		public function pesquisa($municipio_id = null) {
			$this->layout = 'ajax';
			
			$this->set(
				'Ocorrencias',
				$this->OcorrenciaMunicipal->find(
					'all',
					array(
						'conditions' => array(
							'OcorrenciaMunicipal.municipio_id' => $municipio_id
						)
					)
				)
			);
		}
		
		private function _gmaps($type = 'estados', $Programas = null) {
			
			$this->OcorrenciaMunicipal->Localizacao->Behaviors->attach('Containable');
			$this->OcorrenciaMunicipal->Localizacao->contain(
				'OcorrenciaMunicipal',
				'OcorrenciaMunicipal.Municipio',
				'OcorrenciaMunicipal.Municipio.Estado'
			);
			
			$Ocorrencias = $this->OcorrenciaMunicipal->Localizacao->find('all');
			
			$Estados = array();
			$jafoi = array();
			foreach($Ocorrencias as $Ocorrencia) {
				if (!isset($jafoi[$Ocorrencia['OcorrenciaMunicipal']['Municipio']['Estado']['id']])) {
					array_push($Estados, array('Estado'=>$Ocorrencia['OcorrenciaMunicipal']['Municipio']['Estado']));
					$jafoi[$Ocorrencia['OcorrenciaMunicipal']['Municipio']['Estado']['id']] = true;
				}
			}
			
			$this->set('gMapsData', $Estados);

		}
	
		private function _related() {
			$this->Tematica->Behaviors->attach('Containable');
			$this->Tematica->contain('ProgramaTematica');
			$Tematicas = $this->Tematica->find('threaded', array('fields'=>array('id','nome','parent_id'),'order'=>array('Tematica.nome'=>'ASC')));
			//pr($Tematicas);
			$this->set('Tematicas', $Tematicas);
			
			$this->Estado->Behaviors->attach('Containable');
			$this->Estado->contain();
			$Estados = $this->Estado->find('list', array('fields'=>array('id','nome')));
			$this->set('Estados', $Estados);
			
		}
		
	}