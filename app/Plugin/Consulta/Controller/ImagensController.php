<?php
class ImagensController extends ConsultaAppController {
	
	public function marker($estado = null) {
		
		$this->set('estado', strtolower( $estado ) );
		$this->layout = 'image';
		$this->response->type('png');
	}
	
}