var matching = {};
var gIcons0 = {};

mapEstacao = 0; //

gMarkers = {
	'Uf': [],
	'Mu': [],
	'Oc': [],
	'Po': []
};

gInfoWindows0 = new Array();
gWindowContents0 = new Array();

mCount = {
	'Uf': 0,
	'Mu': 0,
	'Oc': 0
};

time = 250;
firstTime = 1000;

$(document).ready(function() {

$('#map-btn-brasil').click(function(){
	setMarkers('Mu', null);
	setMarkers('Oc', null);
	setMarkers('Po', null);
	setMarkers('Uf', mapEstacao);
	mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil
	mapEstacao.setZoom(4);
});

$('#map-btn-localizacao').click(function(){
	setMarkers('Uf', null);
	setMarkers('Mu', null);
	setMarkers('Oc', null);
	setMarkers('Po', null);
	navigator.geolocation.getCurrentPosition(showPosition);
});

//var initialLocation = new google.maps.LatLng(-51, -11);
var browserSupportFlag = new Boolean();
var myOptions = {
	zoom: 4, 
	streetViewControl: false, 
	navigationControl: false, 
	mapTypeControl: false, 
	scaleControl: false, 
	scrollwheel: false, 
	keyboardShortcuts: false, 
	mapTypeId: google.maps.MapTypeId.ROADMAP
};

mapEstacao = new google.maps.Map(
	document.getElementById("home-map"), 
	myOptions
);

loadUfs();

mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil


function loadUfs() {
	for (var Uf in gmData) {
		var image = {
			url: '/consulta/Imagens/marker/'+gmData[Uf]['Estado']['sigla'].toLowerCase(),
			// This marker is 20 pixels wide by 30 pixels tall.
			size: new google.maps.Size(54, 52),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0,0),
			// The anchor for this image is the base of the image at 0,30.
			anchor: new google.maps.Point(27, 52)
		};
		window.setTimeout(
			addMarkerUf,
			((mCount['Uf']+1)*time)+firstTime,
			gmData[Uf]['Estado']['latitude'],
			gmData[Uf]['Estado']['longitude'],
			gmData[Uf]['Estado']['sigla'].toLowerCase(),
			image,
			1
		);
		mCount['Uf']++;
	}
}

function loadMus(loadUf) {
	for (var Uf in gmData) {
		if (gmData[Uf]['Municipio'].length == 0 ) continue;
		if (gmData[Uf]['Estado']['sigla'] != loadUf.toUpperCase()) continue;
		for (var Mu in gmData[Uf]['Municipio']) {
			if (gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'].length == 0 ) continue;
			if (inPlace['Mu'][gmData[Uf]['Municipio']['id']]) continue;
			var image = {
				url: '/consulta/img/logo_ej_marker.png',
				// This marker is 20 pixels wide by 30 pixels tall.
				size: new google.maps.Size(60, 62),
				// The origin for this image is 0,0.
				origin: new google.maps.Point(0,0),
				// The anchor for this image is the base of the image at 0,30.
				anchor: new google.maps.Point(30, 62)
			};
			console.log(image.url);
			window.setTimeout(
				addMarkerMu,
				((mCount['Mu']+1)*time)+firstTime,
				gmData[Uf]['Municipio'][Mu]['latitude'],
				gmData[Uf]['Municipio'][Mu]['longitude'],
				gmData[Uf]['Municipio'][Mu]['id'],
				image,
				gmData[Uf]['Municipio'][Mu]['nome'],
				1
			);
			mCount['Mu']++;
			inPlace['Mu'][gmData[Uf]['Municipio'][Mu]['id']] = true;
		}
	}
	window.setTimeout(
		centerMus,
		((mCount['Mu']+1)*time)+firstTime
	);
}

function loadOcs(loadMu) {
	for (var Uf in gmData) {
		if (gmData[Uf]['Municipio'].length == 0 ) continue;
		if (gmData[Uf]['Estado']['sigla'] != loadMu) continue;
		for (var Mu in gmData[Uf]['Municipio']) {
			if (gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'].length == 0 ) continue;
			if (gmData[Uf]['Municipio'][Mu]['id'] != loadMu) continue;
			for(var Oc in gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal']) {
				var image = {
					url: '/consulta/img/m1.png',
					// This marker is 20 pixels wide by 30 pixels tall.
					size: new google.maps.Size(54, 52),
					// The origin for this image is 0,0.
					origin: new google.maps.Point(0,0),
					// The anchor for this image is the base of the image at 0,30.
					anchor: new google.maps.Point(27, 52)
				};
				window.setTimeout(
					addMarkerOc,
					((mCount['Oc']+1)*time)+firstTime,
					gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'][Oc]['latitude'],
					gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'][Oc]['longitude'],
					gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'][Oc]['id'],
					image,
					gmData[Uf]['Municipio'][Mu]['OcorrenciaMunicipal'][Oc]['nome'],
					1
				);
				mCount['Oc']++;
			}
		}
	}
	window.setTimeout(
		centerMus,
		((mCount['Mu']+1)*time)+firstTime
	);
}

function loadPrograma(id) {
}

function centerMus() {
	console.log(gMarkers['Mu']);
	var bounds = new google.maps.LatLngBounds();
	for (index in gMarkers['Mu']) {
		var data = gMarkers['Mu'][index];
		bounds.extend(new google.maps.LatLng(data.position.lat(), data.position.lng()));
	}
	mapEstacao.fitBounds(bounds);
}

function setMarkers(type, map) {
	
	for (var key in gMarkers[type]) {
		gMarkers[type][key].setMap(map);
	}
	
}

function addMarkerUf(lat, lng, sigla, icon, count_municipios) {
	console.log(lat);
	// Criar Marcador
	var infowindow = new google.maps.InfoWindow({
		content: '<h4>'+sigla.toUpperCase()+'</h4><hr>O Estação Juventude detectou '+count_municipios+' municípios<br> com Programas Sociais no estado do '+sigla.toUpperCase()+'.<hr>Clique na bandeira do estado para vê-los.<br><br>'
	});
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: mapEstacao,
		icon: icon,
		animation: google.maps.Animation.DROP,
		draggable: false,
		sigla: sigla,
		infoWindow: infowindow
	});
	// Listener do Marcador criado
	google.maps.event.addListener(marker, 'click', function(mk) {
		setMarkers('Uf', null);
		loadMus(this.sigla);
	});
	google.maps.event.addListener(marker, 'mouseover', function(mk) {
		infowindow.open(mapEstacao, this);
	});
	google.maps.event.addListener(marker, 'mouseout', function(mk) {
		infowindow.close(mapEstacao, this);
	});
	
	gMarkers['Uf'].push (
		marker
	);
}

function addMarkerMu(lat, lng, id, icon, nome_municipio, count_programas) {
	var infowindow = new google.maps.InfoWindow({
		content: '<h4>'+nome_municipio+'</h4><hr>O Estação Juventude detectou '+count_programas+' Programas Sociais<br> no município de '+nome_municipio+'.<hr>Clique no marcador do município para vê-los.<br><br>'
	});
	// Criar Marcador
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: mapEstacao,
		icon: icon,
		animation: google.maps.Animation.DROP,
		draggable: false,
		infoWindow: infowindow,
		id: id
	});
	// Listener do Marcador criado
	google.maps.event.addListener(marker, 'click', function(mk) {
		setMarkers('Mu', null);
		loadOcs(this.sigla);
	});
	google.maps.event.addListener(marker, 'mouseover', function(mk) {
		infowindow.open(mapEstacao, this);
	});
	google.maps.event.addListener(marker, 'mouseout', function(mk) {
		infowindow.close(mapEstacao, this);
	});
	
	gMarkers['Mu'].push (
		marker
	);
}

function addMarkerOc(lat, lng, id, icon, nome_programa) {
	var infowindow = new google.maps.InfoWindow({
		content: '<h4>'+nome_programa+'</h4><hr>Clique no marcador do programa para vê-los.<br><br>'
	});
	// Criar Marcador
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, lng),
		map: mapEstacao,
		icon: icon,
		animation: google.maps.Animation.DROP,
		draggable: false,
		infoWindow: infowindow,
		id: id
	});
	// Listener do Marcador criado
	google.maps.event.addListener(marker, 'click', function(mk) {
		setMarkers('Oc', null);
		loadPrograma(this.id);
	});
	google.maps.event.addListener(marker, 'mouseover', function(mk) {
		infowindow.open(mapEstacao, this);
	});
	google.maps.event.addListener(marker, 'mouseout', function(mk) {
		infowindow.close(mapEstacao, this);
	});
	
	gMarkers['Mu'].push (
		marker
	);
}

function showPosition(position) {
	var infowindow = new google.maps.InfoWindow({
		content: '<h4>Você</h4><hr>Clique no marcador do município para vê-los.<br><br>'
	});
	var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	var image = {
		url: '/consulta/img/m1.png',
		// This marker is 20 pixels wide by 30 pixels tall.
		size: new google.maps.Size(54, 52),
		// The origin for this image is 0,0.
		origin: new google.maps.Point(0,0),
		// The anchor for this image is the base of the image at 0,30.
		anchor: new google.maps.Point(27, 52)
	};
	var marker = new google.maps.Marker({
		position: userLatLng,
		title: 'Você está aqui!',
		map: mapEstacao,
		infoWindow: infowindow,
		icon: image
	});
	google.maps.event.addListener(marker, 'mouseover', function(mk) {
		infowindow.open(mapEstacao, this);
	});
	google.maps.event.addListener(marker, 'mouseout', function(mk) {
		infowindow.close(mapEstacao, this);
	});
	mapEstacao.setCenter(marker.position);
	mapEstacao.setZoom(12);
	gMarkers['Po'].push (
		marker
	);
}

});