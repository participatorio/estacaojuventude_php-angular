var matching = {};
var gIcons0 = {};
var citySquare = 0;

// Mapas
mapEstacao = 0;

/* Variavel gMarkers contem todos os Markers da Aplicação
** separados pelo seu tipo:
**
** Uf - Estados
** Mu - Municípios
** Oc - Ocorrências
** Lo - Localizações
** Po - Posicão
**
*/

// Marcadores
gMarkers = {
	'Uf': [],
	'Mu': [],
	'Oc': [],
	'Lo': [],
	'Po': []
};

// Contadores
mCount = {
	'Uf': 0,
	'Mu': 0,
	'Oc': 0,
	'Lo': 0
};

// Variaveis
time = 250;
firstTime = 250;

$(document).ready(function() {
	
	function setEvents() {

		$('#map-btn-estados').click(function(){
			/*
			** Reseta o Mapa e carrega os Estados
			**
			*/
			clearAndDeleteAllMarkers();
			loadUfs();
			//mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil
			mapEstacao.setZoom(4);
		});
	
		$('#map-btn-programas').click(function(){
			/*
			** Carrega todos os Estados
			**
			*/
			clearAndDeleteAllMarkers();
			//mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil
			loadOcs('BR', 'All');
			//mapEstacao.setZoom(4);
		});
	
		$('#map-btn-perto li a').click(function(){
			$('#map-btn-perto').data('radius', $(this).data('km'));
			clearAndDeleteAllMarkers();
			navigator.geolocation.getCurrentPosition(showPosition);
		});
		
		$('#map-btn-tematica li a').click(function(){
			clearAndDeleteAllMarkers();
			//mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil
			loadTem($(this).data('tematica-id'));
		});
		
	}
	
	function mapFooter(message) {
		$('#mapFooter').html(message);
	}
	
	function initMap() {
		mapFooter('Inicializando Mapa');
		//var initialLocation = new google.maps.LatLng(-51, -11);
		var browserSupportFlag = new Boolean();
		var myOptions = {
			zoom: 4, 
			streetViewControl: false, 
			navigationControl: false, 
			mapTypeControl: false, 
			scaleControl: false, 
			scrollwheel: false, 
			keyboardShortcuts: false, 
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		
		mapEstacao = new google.maps.Map(
			document.getElementById("home-map"), 
			myOptions
		);

		mapEstacao.setCenter( new google.maps.LatLng(-15,-51)); // Centraliza Brasil

	}

	// Carrega Ufs
	setEvents();
	initMap();
	loadUfs();
	
	function clearAndDeleteAllMarkers() {
		setMarkers('Uf', null);
		deleteMarkers('Uf', null);
		setMarkers('Mu', null);
		deleteMarkers('Mu', null);
		setMarkers('Oc', null);
		deleteMarkers('Oc', null);
		setMarkers('Lo', null);
		deleteMarkers('Lo', null);
		setMarkers('Po', null);
		deleteMarkers('Po', null);
		if (typeof(citySquare) == 'object') citySquare.setMap(null);
		
	}
	
	function clearAndDeleteGeoMarkers() {
		setMarkers('Uf', null);
		deleteMarkers('Uf', null);
		setMarkers('Mu', null);
		deleteMarkers('Mu', null);
		setMarkers('Oc', null);
		deleteMarkers('Oc', null);
		setMarkers('Lo', null);
		deleteMarkers('Lo', null);
		if (typeof(citySquare) == 'object') citySquare.setMap(null);
		
	}
	
	function loadTem(tematica_id) {
		mCount['Oc'] = 0;
		mCount['Lo'] = 0;
		
		clearAndDeleteAllMarkers();
		
		mapFooter('Carregando Programas e Ações do Tema');
		
		$.ajax({
			url: '/consulta/Home/loadTem/'+tematica_id,
			dataType: 'json',
			success: function(gmData) {
				if (gmData.length == 0) {
					bootbox.alert('Nenhum Programa ou Ação foi encontrado!');
				} else {
					for (var Oc in gmData) {
						for (var Lo in gmData[Oc]['Localizacao']) {
							var image = {
								url: '/consulta/img/m1.png',
								size: new google.maps.Size(48, 48),
								origin: new google.maps.Point(0,0),
								anchor: new google.maps.Point(24, 24)
							};
							window.setTimeout(
								addMarkerOc,
								((mCount['Lo']+1)*time)+firstTime,
								{
									'lat': gmData[Oc]['Localizacao'][Lo]['latitude'],
									'lng': gmData[Oc]['Localizacao'][Lo]['longitude'],
									'icon': image, 
									'sigla': gmData[Oc]['Municipio']['Estado']['sigla'],
									'municipio': gmData[Oc]['Municipio']['nome'],
									'programa_id': gmData[Oc]['Programa']['id'],
									'programa_nome': gmData[Oc]['Programa']['nome_divulgacao']
								}
							);
							mCount['Lo']++;
						}
						mCount['Co']++;
					}
					window.setTimeout(
						centerMapMarkers, 
						((mCount['Lo']+1)*time)+firstTime, 
						'Lo'
					);
				}
			}
		});
	}

	function loadUfs() {
		clearAndDeleteAllMarkers();
		
		mapFooter('Carregando Estados com o Programa Estação Juventude');
		
		mCount['Uf'] = 0;
		$.ajax({
			url: '/consulta/Home/loadUfs',
			dataType: 'json',
			success: function(gmData) {
				for (var Uf in gmData) {
					var image = {
						url: '/consulta/Imagens/marker/'+gmData[Uf]['sigla'].toLowerCase(),
						// This marker is 20 pixels wide by 30 pixels tall.
						size: new google.maps.Size(54, 52),
						// The origin for this image is 0,0.
						origin: new google.maps.Point(0,0),
						// The anchor for this image is the base of the image at 0,30.
						anchor: new google.maps.Point(27, 52)
					};
					window.setTimeout(
						addMarkerUf,
						((mCount['Uf']+1)*time)+firstTime,
						gmData[Uf]['latitude'],
						gmData[Uf]['longitude'],
						image,
						{
							'uf_id':gmData[Uf]['id'],
							'uf_sigla':gmData[Uf]['sigla'],
							'count_municipios': 1
						}
					);
					mCount['Uf']++;
				}
			}
		});
		
	}

	function loadMus(estado_id) {
		clearAndDeleteAllMarkers();
		
		mapFooter('Carregando Municípios com o Programa Estação Juventude');
		
		mCount['Mu'] = 0;
		$.ajax({
			url: '/consulta/Home/loadMus/'+estado_id,
			dataType: 'json',
			success: function(gmData) {
				for (var Mu in gmData) {
					var image = {
						url: '/consulta/img/logo_ej_marker.png',
						// This marker is 20 pixels wide by 30 pixels tall.
						size: new google.maps.Size(60, 64),
						// The origin for this image is 0,0.
						origin: new google.maps.Point(0,0),
						// The anchor for this image is the base of the image at 0,30.
						anchor: new google.maps.Point(30, 64)
					};
					window.setTimeout(
						addMarkerMu,
						((mCount['Mu']+1)*time)+firstTime,
						gmData[Mu]['latitude'],
						gmData[Mu]['longitude'],
						image,
						{
							'estado_sigla': gmData[Mu]['Estado']['sigla'].toLowerCase(),
							'municipio_id': gmData[Mu]['id'],
							'municipio_nome': gmData[Mu]['nome'],
							'count_programas': 1
						}
					);
					mCount['Mu']++;
				}
			}
		});
	}

	function loadOcs(sigla, municipio_id) {
		clearAndDeleteAllMarkers();
		
		mapFooter('Carregando Programas e Ações');
		
		mCount['Oc'] = 0;
		mCount['Lo'] = 0;
		
		$.ajax({
			url: '/consulta/Home/loadOcs/'+sigla+'/'+municipio_id,
			dataType: 'json',
			beforeStart: $('#pleaseWaitDialog').modal('show'),
			complete: $('#pleaseWaitDialog').modal('hide'),
			success: function(gmData) {
				if (gmData.length == 0) {
					bootbox.alert('Nenhum Programa ou Ação foi encontrado!');
				} else {
					for (var Oc in gmData) {
						for (var Lo in gmData[Oc]['Localizacao']) {
							var image = {
								url: '/consulta/img/m1.png',
								size: new google.maps.Size(48, 48),
								origin: new google.maps.Point(0,0),
								anchor: new google.maps.Point(24, 24)
							};
							window.setTimeout(
								addMarkerOc,
								((mCount['Lo']+1)*time)+firstTime,
								{
									'lat': gmData[Oc]['Localizacao'][Lo]['latitude'],
									'lng': gmData[Oc]['Localizacao'][Lo]['longitude'],
									'icon': image, 
									'sigla': gmData[Oc]['Municipio']['Estado']['sigla'],
									'municipio': gmData[Oc]['Municipio']['nome'],
									'programa_id': gmData[Oc]['Programa']['id'],
									'programa_nome': gmData[Oc]['Programa']['nome_divulgacao'],
									'beneficios': gmData[Oc]['OcorrenciaMunicipal']['beneficios_locais'],
									'ocorrencia_municipal_id': gmData[Oc]['OcorrenciaMunicipal']['id'],
									'endereco': gmData[Oc]['Localizacao'][Lo]['endereco'],
									'data_inicio': gmData[Oc]['OcorrenciaMunicipal']['inicio_inscricoes'],
									'data_fim': gmData[Oc]['OcorrenciaMunicipal']['fim_inscricoes']
	
								}
							);
							mCount['Lo']++;
						}
						mCount['Co']++;
					}
					window.setTimeout(
						centerMapMarkers, 
						((mCount['Lo']+1)*time)+firstTime, 
						'Lo'
					);
				}
			}
		});
	}

	function loadOcorrencia(id) {
		window.open('/consulta/Programa/view/'+id, '_blank');
	}

	function centerMapMarkers(type) {
		if (gMarkers[type].length == 0) return;
		var bounds = new google.maps.LatLngBounds();
		for (index in gMarkers[type]) {
			var data = gMarkers[type][index];
			bounds.extend(new google.maps.LatLng(data.position.lat(), data.position.lng()));
		}
		mapEstacao.fitBounds(bounds);
	}

	function setMarkers(type, map) {
		
		for (var key in gMarkers[type]) {
			gMarkers[type][key].setMap(map);
		}
	}
	
	function deleteMarkers(type, map) {
		
		for (var key in gMarkers[type]) {
			gMarkers[type][key].setMap(map);
		}
		gMarkers[type].length = 0;
	}

	function addMarkerUf(lat, lng, icon, options) {
		// Criar Marcador
		var infowindow = new google.maps.InfoWindow({
			content: '<h4>'+options['uf_sigla'].toUpperCase()+'</h4><hr>O Estação Juventude informa '+options['count_municipios']+' municípios com Programa Estação Juventude no estado do '+options['uf_sigla'].toUpperCase()+'.<hr>Clique na bandeira do estado para vê-los.<br><br>'
		});
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: mapEstacao,
			icon: icon,
			animation: google.maps.Animation.DROP,
			draggable: false,
			sigla: options['uf_sigla'],
			id: options['uf_id'],
			infoWindow: infowindow
		});
		// Listener do Marcador criado
		google.maps.event.addListener(marker, 'click', function(mk) {
			infowindow.close(mapEstacao, this);
			setMarkers('Uf', null);
			loadMus(this.id);
		});
		google.maps.event.addListener(marker, 'mouseover', function(mk) {
			infowindow.open(mapEstacao, this);
		});
		google.maps.event.addListener(marker, 'mouseout', function(mk) {
			infowindow.close(mapEstacao, this);
		});
		
		gMarkers['Uf'].push (
			marker
		);
	}
	
	function addMarkerMu(lat, lng, icon, options) {
		var infowindow = new google.maps.InfoWindow({
			content: '<h4>'+options['municipio_nome']+'</h4><hr>O Estação Juventude detectou '+options['count_programas']+' Programas Sociais<br> no município de '+options['municipio_nome']+'.<hr>Clique no marcador do município para vê-los.<br><br>'
		});
		// Criar Marcador
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: mapEstacao,
			icon: icon,
			animation: google.maps.Animation.DROP,
			draggable: false,
			infoWindow: infowindow,
			id: options['municipio_id'],
			sigla: options['estado_sigla']
		});
		// Listener do Marcador criado
		google.maps.event.addListener(marker, 'click', function(mk) {
			infowindow.close(mapEstacao, this);
			setMarkers('Mu', null);
			loadOcs(this.sigla, this.id);
		});
		google.maps.event.addListener(marker, 'mouseover', function(mk) {
			infowindow.open(mapEstacao, this);
		});
		google.maps.event.addListener(marker, 'mouseout', function(mk) {
			infowindow.close(mapEstacao, this);
		});
		
		gMarkers['Mu'].push (
			marker
		);
	}
	
	function addMarkerOc(options) {
		var infowindow = new google.maps.InfoWindow({
			content: 
				'<h4>'+
				options['programa_nome']+
				'</h4><hr>'+
				options['beneficios']+
				'<br>'+
				options['endereco']+
				'<br>'+
				'Início: '+options['data_inicio']+
				'<br>'+
				'Fim: '+options['data_fim']+
				'<hr>Clique no marcador do programa para vê-los.<br><br>'
		});
		// Criar Marcador
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(options['lat'], options['lng']),
			map: mapEstacao,
			icon: options['icon'],
			animation: google.maps.Animation.DROP,
			draggable: false,
			infoWindow: infowindow
		});
		// Listener do Marcador criado
		google.maps.event.addListener(marker, 'click', function(mk) {
			setMarkers('Oc', null);
			loadOcorrencia(options['ocorrencia_municipal_id']);
		});
		google.maps.event.addListener(marker, 'mouseover', function(mk) {
			infowindow.open(mapEstacao, this);
		});
		google.maps.event.addListener(marker, 'mouseout', function(mk) {
			infowindow.close(mapEstacao, this);
		});
		
		gMarkers['Lo'].push (
			marker
		);
	}
	
	function geoAddress() {
		$('#geo-address-btn').click(function(){
			carregarNoMapa($('#geo-address').val());
		});
	}
			
	function carregarNoMapa(endereco) {
		clearAndDeleteGeoMarkers();
		geocoder = new google.maps.Geocoder;
		var userLatLng = 0;
		geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();

					userLatLng = new google.maps.LatLng(latitude, longitude);
					gMarkers['Po'][0].setPosition(userLatLng);
					mapEstacao.setCenter(userLatLng);
					//mapEstacao.setZoom(15);
					
					SquareAndOcs( userLatLng );
				}
			}
		});
	}
	
	function SquareAndOcs(userLatLng) {
		
		var radius = $('#map-btn-perto').data('radius');
		
		var populationOptions = {
		  strokeColor: "#0000FF",
		  strokeOpacity: 0.8,
		  strokeWeight: 2,
		  fillColor: "#0000FF",
		  fillOpacity: 0.35,
		  map: mapEstacao,
		  bounds: new google.maps.LatLngBounds(
		  	new google.maps.LatLng(userLatLng.lat() - radius/111.11, userLatLng.lng() - radius/111.11),
		  	new google.maps.LatLng(userLatLng.lat() + radius/111.11, userLatLng.lng() + radius/111.11)
		  )
		};
		if (typeof(citySquare) == 'object') citySquare.setMap(null);
		citySquare = new google.maps.Rectangle(populationOptions);
		
		$.ajax({
			url: '/consulta/Home/loadRadius/',
			dataType: 'json',
			method: 'post',
			data: {'lat': userLatLng.k, 'lng': userLatLng.B, 'radius':radius},
			success: function(gmData) {
				if (gmData.length == 0) {
					bootbox.alert('Nenhum Programa ou Ação foi encontrado!');
				} else {
					for (var Oc in gmData) {
						for (var Lo in gmData[Oc]['Localizacao']) {
							var image = {
								url: '/consulta/img/m1.png',
								size: new google.maps.Size(48, 48),
								origin: new google.maps.Point(0,0),
								anchor: new google.maps.Point(24, 24)
							};
							window.setTimeout(
								addMarkerOc,
								((mCount['Lo']+1)*time)+firstTime,
								{
									'lat': gmData[Oc]['Localizacao'][Lo]['latitude'],
									'lng': gmData[Oc]['Localizacao'][Lo]['longitude'],
									'icon': image, 
									'sigla': gmData[Oc]['Municipio']['Estado']['sigla'],
									'municipio': gmData[Oc]['Municipio']['nome'],
									'programa_id': gmData[Oc]['Programa']['id'],
									'programa_nome': gmData[Oc]['Programa']['nome_oficial'],
									'beneficios': gmData[Oc]['OcorrenciaMunicipal']['beneficios_locais'],
									'ocorrencia_municipal_id': gmData[Oc]['OcorrenciaMunicipal']['id'],
									'endereco': gmData[Oc]['Localizacao'][Lo]['endereco'],
									'data_inicio': gmData[Oc]['OcorrenciaMunicipal']['inicio_inscricoes'],
									'data_fim': gmData[Oc]['OcorrenciaMunicipal']['fim_inscricoes']
								}
							);
							mCount['Lo']++;
						}
						mCount['Co']++;
					}
					window.setTimeout(
						centerMapMarkers, 
						((mCount['Lo']+1)*time)+firstTime, 
						'Lo'
					);
				}
			}
		});
	}
	
	function showPosition(position) {
		clearAndDeleteAllMarkers();
		
		mapFooter('Endereço<br><input type="text" id="geo-address"><button class="btn btn-default" id="geo-address-btn">Carregar</button>');
		
		geoAddress();
		
		var radius = $('#map-btn-perto').data('radius');
		var infowindow = new google.maps.InfoWindow({
			content: '<h4>Você</h4><hr>Sua posição atual.<br><br>'
		});
		var userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var image = {
			url: '/consulta/img/bandeiras/markers/user.png',
			// This marker is 20 pixels wide by 30 pixels tall.
			size: new google.maps.Size(50, 50),
			// The origin for this image is 0,0.
			origin: new google.maps.Point(0,0),
			// The anchor for this image is the base of the image at 0,30.
			anchor: new google.maps.Point(25, 25)
		};
		var marker = new google.maps.Marker({
			position: userLatLng,
			title: 'Você está aqui!',
			map: mapEstacao,
			infoWindow: infowindow,
			icon: image
		});
		google.maps.event.addListener(marker, 'mouseover', function(mk) {
			infowindow.open(mapEstacao, this);
		});
		google.maps.event.addListener(marker, 'mouseout', function(mk) {
			infowindow.close(mapEstacao, this);
		});
		mapEstacao.setCenter(marker.position);
		mapEstacao.setZoom(7);
		gMarkers['Po'].push (
			marker
		);
		
		SquareAndOcs(userLatLng);

	}

});