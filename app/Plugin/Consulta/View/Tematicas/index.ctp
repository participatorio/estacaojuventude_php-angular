<?php
$this->extend('Bootstrap.Common/index');
$this->assign('pageHeader', 'Temáticas');
$this->assign('panelStyle', 'primary');

$this->assign('actions', $this->Bootstrap->actions(null, array(
	array(
		'text' => 'Adicionar',
		'icon' => 'plus',
		'style' => 'info',
		'action' => 'add',
		'controller' => 'Tematicas'
	)
)));
?>

<?php $this->start('table-tr'); ?>
	<tr>
		<th>Nome</th>
		<th>Pai</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
	<?php foreach($data as $Ministerio) { ?>
	<tr>
		<td><?php echo $Tematica['Tematica']['nome']; ?></td>
		<td><?php echo $Tematica['TopTematica']['nome']; ?>
	<tr>
	<?php } ?>
<?php $this->end(); ?>