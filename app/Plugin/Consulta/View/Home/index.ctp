<?php
$this->extend('Consulta.Home/templates/home');
$this->assign('pageHeader', 'Programas');
$this->assign('panelStyle', 'primary');
?>
<?php $this->start('temas'); ?>
	<div class="temas">
		<?php foreach($Tematicas as $key=>$value) { ?>
			<div data-val="<?php echo $key;?>" class="tema-vagao btn-tema-programa">
				<?php if (!empty($Tema['children'])) { ?>
				<i data-tema="<?php echo $key;?>" class="show-subtema glyphicon glyphicon-chevron-right pull-right"></i>
				<?php } ?>
				&nbsp;<span class="text" style="padding:2px 8px;"><?php echo $this->Bootstrap->title_case( $value );?></span>
			</div>
		<?php } ?>
	</div>
<?php $this->end(); ?>
<?php $this->start('conteudo'); ?>

	<?php if (!empty($Programas)) { ?>
		<?php foreach($Programas as $Programa) { ?>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo $Programa['Programa']['nome_divulgacao'];?></div>
				<div class="panel-body triangulos">
					<p><?php echo $Programa['Programa']['descricao'];?></p>
					<p><?php echo $Programa['Programa']['criterios_acesso'];?></p>
					<p><?php echo $Programa['Programa']['beneficios'];?></p>
					<br>
					<p>Início: <?php echo $Programa['OcorrenciaMunicipal']['inicio_inscricoes']; ?></p>
					<p>Fim: <?php echo $Programa['OcorrenciaMunicipal']['fim_inscricoes']; ?></p>
					<br>
					<?php echo $this->Bootstrap->btnLink('Ver mais', array(
						'action'=>'view',
						$Programa['Programa']['id']
					),
					array(
						'style' => 'success'
					)
					); ?>
				</div>
			</div>	
		<?php } ?>
	<?php } ?>
	<?php $this->end(); ?>
	
<?php $this->start('gmaps'); ?>
	<div id="home-map" style="width:100%; height: 500px;"></div>
	<?php echo $this->Html->script('Consulta.gmaps'); ?>
<?php $this->end(); ?>

