<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>

<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p>Processando...</p>
      </div>
    </div>
  </div>
</div>
		    
<div class="row bg-white-semi">
	<div class="col-md-5">
		<br>
		<?php echo $this->Html->link($this->Html->image('Consulta.logo_ej_header_50.png'), '/',array('escape'=>false)); ?>
	</div>
	<div class="col-md-7">
		<br><br>
		<ul class="nav nav-pills pull-right" role="tablist">
			<li role="presentation" class="<?php echo (isset($isLocalizacao))?('active'):('');?>"><a href="#gmaps" role="tab" data-toggle="tab">Geo-Localização</a></li>
			<li role="presentation" class="<?php echo (isset($isPrograma))?('active'):('');?>"><a href="#programas" role="tab" data-toggle="tab">Pesquisa</a></li>
		</ul>
	</div>
</div>
<br>
<div class="tab-content">
	<div class="tab-pane <?php echo (isset($isLocalizacao))?('active'):('');?>" id="gmaps">
		<div class="panel panel-default">
			<div class="panel-heading">
					<div class="btn-group">
					<button type="button" class="btn btn-default" id="map-btn-estados">
						<span class="glyphicon glyphicon-flag"></span> Estados
					</button>
					<button type="button" class="btn btn-default" id="map-btn-programas">
						<span class="glyphicon glyphicon-star"></span> Programas
					</button>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-map-marker"></span> Perto de você! <span class="caret"></span>
					</button>
					<ul id="map-btn-perto" class="dropdown-menu" role="menu">
						<li><a data-km="5" href="#">até 5 Km</a></li>
						<li><a data-km="10" href="#">até 10 Km</a></li>
						<li><a data-km="25" href="#">até 25 Km</a></li>
						<li><a data-km="50" href="#">até 50 Km</a></li>
						<li><a data-km="75" href="#">até 75 Km</a></li>
						<li><a data-km="100" href="#">até 100 Km</a></li>
					</ul>
					</div>
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							Escolha uma Temática <span class="caret"></span>
						</button>
						<ul id="map-btn-tematica" class="dropdown-menu" role="menu">
							<?php foreach($Tematicas as $key=>$value) { ?>
							<li><a data-tematica-id="<?php echo $key; ?>" href="#"><?php echo $value; ?></a></li>
							<?php } ?>
						</ul>
					</div>
			</div>
			<?php echo $this->fetch('gmaps'); ?>
			<div class="panel-footer" id="mapFooter"></div>
		</div>
	</div>
	<div class="tab-pane <?php echo (isset($isPrograma))?('active'):('');?>" id="programas">
					

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Temáticas</div>
			<div class="panel-body">
				<?php echo $this->fetch('temas'); ?>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12" id="programas-container">
				<div class="panel panel-primary">
					<div class="panel-heading">Pesquisa</div>
					<div class="panel-body">
						<form id="formPesquisa" method="post">
							<input type="hidden" name="data[Programa][tema_id]" id="ProgramaTemaId">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<select name="data[Programa][estado_id]" class="form-control" id="pesquisar-estado">
											<option value="0">Todos os Estados</option>
											<?php foreach($Estados as $key=>$value) { ?>
											<option value="<?php echo $key;?>"><?php echo $value; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<select name="data[Programa][municipio_id]" class="form-control" id="pesquisar-municipio" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">
											<option value="0">Todos os Municípios do Estado</option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<button class="btn btn-default" id="btn-pesquisar-programas" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">Pesquisar</button>
								</div>
							</div>
						</form>
						<div id="pesquisa-div"></div>
					</div>
				</div>
				<?php echo $this->fetch('conteudo'); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.btn-tema-programa').click(function(){
			if($(this).data('selected') == '1') {
				$(this).removeClass('list-group-item-success');
				$(this).data('selected', 0);
			} else {
				$(this).addClass('list-group-item-success');
				$(this).data('selected', 1);
			}
		});
		$('.show-subtema').click(function(){
			if ($('.subtemas-'+$(this).data('tema')).hasClass('slideUp')) {
				$('.subtemas-'+$(this).data('tema')).slideDown().removeClass('slideUp');
			} else {
				$('.subtemas-'+$(this).data('tema')).slideUp().addClass('slideUp');
			}
		});
		/*
		$('#btn-pesquisar-programas').click(function(){
			$('#programas-container').html('');
			$('#btn-pesquisar-programas').popover('show');
			temas = '0';
			$('span.glyphicon-ok-circle').each(function(){
				temas += ','+$(this).data('value');
			});
			$.ajax({
				'url': '/consulta/Programas/pesquisar',
				'type': 'post',
				'data': {'temas':temas,'estado':$('#pesquisar-estado').val(),'municipio':$('#pesquisar-municipio').val()},
				'success': function(data) {
					$('#btn-pesquisar-programas').popover('hide');
					$('#programas-container').html(data);
				}
			});
		});
		*/
		$('#btn-pesquisar-programas').click(function(){
			valores = [];
			$('.list-group-item-info').each(function(){
				valores.push($(this).data('val'));
			});
			$('#ProgramaTemaId').val(valores.toString());
			
			$.ajax({
				url: '/consulta/Ocorrencia/pesquisa/'+$('#pesquisar-municipio').val(),
				success: function(data) {
					$('#pesquisa-div').html(data);
				}
			});
			
			return false;
		});
		
		$('#pesquisar-estado').change(function() {
			$('#pesquisar-municipio').popover('show');
			$.ajax({
				'url': '/consulta/Programa/municipios/'+$(this).val(),
				'type':'post',
				'success': function(data) {
					$('#pesquisar-municipio').popover('hide');
					$('#pesquisar-municipio').html(data);
				}
			})
		});
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$('#gmaps').html($('#gmaps').html());
		});
		
	});
</script>
