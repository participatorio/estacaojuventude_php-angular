<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="btn-group nav nav-tabs" role="tablist">
	<a class="btn btn-default" href="#gmaps" role="tab" data-toggle="tab">Geo-Localização</a>
	<a class="btn btn-default" href="#programas" role="tab" data-toggle="tab">Pesquisa</a>
</div>
<hr>
<div class="tab-content">
	<div class="tab-pane <?php echo (isset($isLocalizacao))?('active'):('');?>" id="gmaps">
		<div class="panel panel-default">
			<div class="panel-heading">Google Maps</div>
			<?php echo $this->fetch('gmaps'); ?>
			<div class="panel-footer">
				<div class="btn-group">
					<button class="btn btn-default" id="map-btn-localizacao"><span class="glyphicon glyphicon-map-marker"></span>&nbsp;Perto de Você!</button>
					<button class="btn btn-default" id="map-btn-brasil">&nbsp;Brasil</button>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane <?php echo (isset($isPrograma))?('active'):('');?>" id="programas">
					

<div class="row">
	<div class="col-md-4">
		<?php echo $this->fetch('temas'); ?>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12" id="programas-container">
				<br>
					<form id="formPesquisa" method="post">
						<input type="hidden" name="data[Programa][tema_id]" id="ProgramaTemaId">
						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<select name="data[Programa][estado_id]" class="form-control" id="pesquisar-estado">
										<option value="0">Todos os Estados</option>
										<?php foreach($Estados as $key=>$value) { ?>
										<option value="<?php echo $key;?>"><?php echo $value; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-5">
								<div class="form-group">
									<select name="data[Programa][municipio_id]" class="form-control" id="pesquisar-municipio" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">
										<option value="0">Todos os Municípios do Estado</option>
									</select>
								</div>
							</div>
							<div class="col-md-2">
								<button class="btn btn-default" id="btn-pesquisar-programas" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">Pesquisar</button>
							</div>
						</div>
					</form>
					<?php echo $this->fetch('conteudo'); ?>
				</div>
			</div>
			
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.btn-tema-programa .text').click(function(){
			if($(this).data('selected') == '1') {
				$(this).removeClass('list-group-item-info');
				$(this).data('selected', 0);
			} else {
				$(this).addClass('list-group-item-info');
				$(this).data('selected', 1);
			}
		});
		$('.show-subtema').click(function(){
			if ($('.subtemas-'+$(this).data('tema')).hasClass('slideUp')) {
				$('.subtemas-'+$(this).data('tema')).slideDown().removeClass('slideUp');
			} else {
				$('.subtemas-'+$(this).data('tema')).slideUp().addClass('slideUp');
			}
		});
		/*
		$('#btn-pesquisar-programas').click(function(){
			$('#programas-container').html('');
			$('#btn-pesquisar-programas').popover('show');
			temas = '0';
			$('span.glyphicon-ok-circle').each(function(){
				temas += ','+$(this).data('value');
			});
			$.ajax({
				'url': '/consulta/Programas/pesquisar',
				'type': 'post',
				'data': {'temas':temas,'estado':$('#pesquisar-estado').val(),'municipio':$('#pesquisar-municipio').val()},
				'success': function(data) {
					$('#btn-pesquisar-programas').popover('hide');
					$('#programas-container').html(data);
				}
			});
		});
		*/
		$('#btn-pesquisar-programas').click(function(){
			valores = [];
			$('.list-group-item-info').each(function(){
				valores.push($(this).data('val'));
			});
			$('#ProgramaTemaId').val(valores.toString());
			$('form').get(0).submit();
			return false;
		});
		
		$('#pesquisar-estado').change(function() {
			$('#pesquisar-municipio').popover('show');
			$.ajax({
				'url': '/consulta/Programa/municipios/'+$(this).val(),
				'type':'post',
				'success': function(data) {
					$('#pesquisar-municipio').popover('hide');
					$('#pesquisar-municipio').html(data);
				}
			})
		});
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$('#gmaps').html($('#gmaps').html());
		});
		
	});
</script>
