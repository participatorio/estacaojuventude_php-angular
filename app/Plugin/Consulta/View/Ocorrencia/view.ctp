<?php
$this->extend('Bootstrap.Common/view');
$this->assign('pageHeader', 'Programa - '.$Programa['Programa']['nome_oficial']);
$this->assign('panelStyle', 'primary');
?>
<?php $this->start('bread'); ?>
<ol class="breadcrumb">
  <li><a href="/">Programas</a></li>
  <li class="active">Programa - <?php echo $Programa['Programa']['nome_oficial'];?></li>
</ol>
<?php $this->end(); ?>
<?php $this->start('body'); ?>
	<p><span class="text-primary">Nome e Sigla</span><br>
		<?php echo $Programa['Programa']['nome_divulgacao'];?> - 
		<?php echo $Programa['Programa']['sigla'];?>
	</p>
	<p><span class="text-primary">Programa Vinculado</span><br><?php echo $Programa['ProgramaVinculado']['nome_oficial'];?></p>
	
	<p><span class="text-primary">Descrição</span><br><?php echo $Programa['Programa']['descricao'];?></p>
	<p><span class="text-primary">Objetivos</span><br><?php echo $Programa['Programa']['objetivos'];?></p>
	<p><span class="text-primary">Benefícios</span><br><?php echo $Programa['Programa']['beneficios'];?></p>
	<p><span class="text-warning">Critérios</span><br><?php echo $Programa['Programa']['criterios_acesso'];?></p>
	<p><span class="text-primary">Data de Início</span><br><?php echo date('d/m/Y',strtotime($Programa['Programa']['data_inicio']));?></p>
	<p><span class="text-primary">Duração</span><br><?php echo $Programa['Programa']['duracao'];?></p>
	<p><span class="text-primary">Público Alvo</span><br><?php echo $Programa['Programa']['publico_alvo'];?></p>
	<p><span class="text-primary">Critérios de Acesso</span><br><?php echo $Programa['Programa']['criterios_acesso'];?></p>
	<p><span class="text-primary">Temporalidade</span><br><?php echo $Programa['Programa']['temporalidade'];?></p>
	
	<p><span class="text-primary">Idade</span><br>
		De <?php echo $Programa['Programa']['idade_minima'];?> a <?php echo $Programa['Programa']['idade_maxima'];?> anos
	</p>
	<p><span class="text-primary">Lei de Criação</span><br><?php echo $Programa['Programa']['lei_criacao'];?></p>
	<p><span class="text-primary">Nível</span><br><?php echo $Programa['Programa']['nivel'];?></p>
	<p><span class="text-primary">Parceiros</span><br><?php echo $Programa['Programa']['parceiros'];?></p>
	
	
<?php $this->end(); ?>
<script>
FB.XFBML.parse();
gapi.plusone.go();
</script>