<?php
$this->extend('Consulta.Ocorrencia/home');
$this->assign('pageHeader', 'Programas');
$this->assign('panelStyle', 'primary');
?>
<?php $this->start('bread'); ?>
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Programas</li>
</ol>
<?php $this->end(); ?>
<?php $this->start('temas'); ?>
	<div class="temas">
		<?php foreach($Tematicas as $Tema) { ?>
			<div data-val="<?php echo $Tema['Tematica']['id'];?>" class="tema-vagao btn-tema-programa">
				<?php if (!empty($Tema['children'])) { ?>
					<i data-tema="<?php echo $Tema['Tematica']['id'];?>" class="show-subtema glyphicon glyphicon-chevron-right pull-right"></i>
				<?php } ?>
				<span class="badge pull-left"><?php echo count($Tema['ProgramaTematica']);?></span>
				&nbsp;<span class="text" style="padding:2px 8px;"><?php echo $this->Bootstrap->title_case( $Tema['Tematica']['nome'] );?></span>
			</div>
			<?php if (!empty($Tema['children'])) { ?>
				<div style="margin-left: 12px; display: none;" class="slideUp subtemas-<?php echo $Tema['Tematica']['id'];?>">
					<?php foreach($Tema['children'] as $SubTema) { ?>
						<div data-val="<?php echo $SubTema['Tematica']['id'];?>" class="tema-vagao btn-tema-programa">
							<span class="badge pull-left"><?php echo count($SubTema['ProgramaTematica']);?></span>
							&nbsp;<span class="text" style="padding:2px 8px;"><?php echo $this->Bootstrap->title_case( $SubTema['Tematica']['nome'] );?></span>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
<?php $this->end(); ?>
<?php $this->start('conteudo'); ?>

	<?php if (!empty($Programas)) { ?>
		<?php foreach($Programas as $Programa) { ?>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo $Programa['Programa']['nome_divulgacao'];?></div>
				<div class="panel-body triangulos">
					<p><?php echo $Programa['Programa']['descricao'];?></p>
					<p><?php echo $Programa['Programa']['criterios_acesso'];?></p>
					<p><?php echo $Programa['Programa']['beneficios'];?></p>
					<br>
					<p>Início: <?php echo $Programa['OcorrenciaMunicipal']['inicio_inscricoes']; ?></p>
					<p>Fim: <?php echo $Programa['OcorrenciaMunicipal']['fim_inscricoes']; ?></p>
					<br>
					<?php echo $this->Bootstrap->btnLink('Ver mais', array(
						'action'=>'view',
						$Programa['Programa']['id']
					),
					array(
						'style' => 'success'
					)
					); ?>
				</div>
			</div>	
		<?php } ?>
	<?php } ?>
	<?php $this->end(); ?>
	<?php $this->start('gmaps'); ?>
		<?php //pr($gMapsData); ?>
		<div id="home-map" style="width:100%; height: 550px;"></div>

		<script type="text/javascript">
			var gmData = <?php echo json_encode($gMapsData); ?>;
		</script>
		<?php echo $this->Html->script('Consulta.gmaps2'); ?>
	<?php $this->end(); ?>

