<?php
$this->extend('Bootstrap.Common/index');
$this->assign('pageHeader', 'Ministérios');
$this->assign('panelStyle', 'primary');

/*
$this->assign('actions', $this->Bootstrap->actions(null, array(
	array(
		'text' => 'Adicionar',
		'icon' => 'plus',
		'style' => 'info',
		'action' => 'add',
		'controller' => 'Ministerios'
	)
)));
*/
$this->assign('actions', false);
?>

<?php $this->start('table-tr'); ?>
	<tr>
		<th>Nome</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
	<?php foreach($data as $Ministerio) { ?>
	<tr>
		<td><?php echo $Ministerio['Ministerio']['nome']; ?></td>
	<tr>
	<?php } ?>
<?php $this->end(); ?>