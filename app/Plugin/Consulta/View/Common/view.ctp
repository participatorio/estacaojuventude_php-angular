<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<br>
<div class="panel panel-<?php echo $panelStyle;?>">
	<div class="panel-heading"><h3 class="panel-title">Detalhes do Programa</h3></div>
	<div class="panel-body">
		<?php echo $this->fetch('body'); ?>
	</div>
</div>
