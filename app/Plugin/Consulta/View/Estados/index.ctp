<?php
$this->extend('Bootstrap.Common/index');
$this->assign('pageHeader', 'Estados');
$this->assign('panelStyle', 'primary');

$this->assign('actions', false);
?>

<?php $this->start('table-tr'); ?>
	<tr>
		<th>Nome</th>
		<th>Sigla</th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
	<?php foreach($data as $Estado) { ?>
	<tr>
		<td><?php echo $Estado['Estado']['nome']; ?></td>
		<td><?php echo $Estado['Estado']['sigla']; ?></td>
	<tr>
	<?php } ?>
<?php $this->end(); ?>