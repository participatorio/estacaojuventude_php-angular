<script>
			function putMarker(municipio, latlng, id) {
				markers_m = [];
				i = markers_m.length;
				markers_m[i] = new google.maps.Marker({
					id: 'marker'+municipio,
					position: new google.maps.LatLng(latlng[1],latlng[0]),
					map: gmap,
					municipio: municipio,
					title: municipio,
					icon: '/img/marker_triangle.png',
					labelContent: 'Programas do município '+estado
				});
				google.maps.event.addListener(markers_m[i], 'click', function() {
					clickMunicipio(this.municipio);
				});
			}
</script>
<?php
foreach($Municipios as $Municipio) { ?>
	<script>
	latlng = [
		'latitude' => <?php echo $Municipio['Municipio']['latitude']; ?>,
		'longitude' => <?php echo $Municipio['Municipio']['longitude']; ?> 
	];
	if (latlng) {
		putMarker('<?php echo $Municipio['Municipio']['nome']; ?>', latlng, <?php echo$Municipio['Municipio']['id'];?>);
	} else {
		putMarker('<?php echo $Municipio['Municipio']['nome']; ?>', [0,0], <?php echo $Municipio['Municipio']['id'];?>);
		/*
		echo $this->GoogleMap->addMarker(
			'gmap', 
			'marker'.$Municipio['Municipio']['id'], 
			$Municipio['Municipio']['nome'].', '.$Municipio['Estado']['sigla'].', Brazil', 
			array(
				'markerIcon' => 'marker_triangle.png',
				'pureScript' => true,
				'windowText' => $Municipio['Municipio']['nome'],
				'markerTitle' => $Municipio['Municipio']['nome']
			)
		);
		*/
	}
	</script>
<?php } ?>
	