<div>
	<p class="clearfix"><?php echo $this->Bootstrap->fixedPaginator(); ?></p>
	<?php foreach($data as $Programa) { ?>
	<div class="panel panel-primary">
		<div class="panel-heading">
			<?php echo $Programa['Programa']['nome_oficial'];?>
			<span class="pull-right label label-warning"><?php echo $Programa['Tematica']['nome']; ?></span>
		</div>
		<div class="panel-body clearfix">
			<p><span class="text-primary">Descrição</span><br><?php echo $Programa['Programa']['descricao'];?></p>
			<p><span class="text-primary">Objetivos</span><br><?php echo $Programa['Programa']['objetivos'];?></p>
			<p><span class="text-primary">Benefícios</span><br><?php echo $Programa['Programa']['beneficios'];?></p>
			<p><span class="text-warning">Critérios</span><br><?php echo $Programa['Programa']['criterios_acesso'];?></p>
			<p><span class="text-primary">Data de Início</span><br><?php echo date('d/m/Y',strtotime($Programa['Programa']['data_inicio']));?></p>
			<p><span class="text-primary">Duração</span><br><?php echo $Programa['Programa']['duracao'];?></p>
		</div>
		<div class="panel-footer">
			<a href="/Programas/view/<?php echo $Programa['Programa']['id'];?>" class="btn btn-success btn-sm">Leia mais...</a>
			&nbsp;
		</div>
	</div>
	<?php } ?>
</div>

