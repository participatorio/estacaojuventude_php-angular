<?php
$this->extend('Consulta.Common/view');
$this->assign('pageHeader', $Ocorrencia['Programa']['nome_oficial']);
$this->assign('panelStyle', 'default');
?>
<?php $this->start('body'); ?>
	<?php //pr($Ocorrencia); ?>
	<h3>
	<?php echo $Ocorrencia['Programa']['nome_divulgacao'];?> - 
	<?php echo $Ocorrencia['Programa']['sigla'];?>
	</h3>
	<p><?php echo $Ocorrencia['Programa']['descricao'];?></p>
	<?php if (isset($Ocorrencia['Programa']['ProgramaVinculado']['nome_oficial'])) { ?>
	<h4>Programa Vinculado: <small><?php echo $Ocorrencia['Programa']['ProgramaVinculado']['nome_oficial'];?></small></h4>
	<?php } ?>
	<h4>Início: <small><?php echo $Ocorrencia['OcorrenciaMunicipal']['inicio_inscricoes'];?></small></h4>
	<h4>Duração: <small><?php echo $Ocorrencia['Programa']['duracao'];?></small></h4>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default"><div class="panel-heading">Objetivos</div><div class="panel-body"><?php echo $Ocorrencia['Programa']['objetivos'];?></div></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-warning"><div class="panel-heading">Critérios</div><div class="panel-body"><?php echo $Ocorrencia['Programa']['criterios_acesso'];?></div></div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary"><div class="panel-heading">Benefícios</div><div class="panel-body"><?php echo $Ocorrencia['Programa']['beneficios'];?></div></div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-success"><div class="panel-heading">Idade</div><div class="panel-body">De <?php echo $Ocorrencia['Programa']['idade_minima'];?> a <?php echo $Ocorrencia['Programa']['idade_maxima'];?> anos</div></div>
		</div>
	</div>
<?php $this->end(); ?>
