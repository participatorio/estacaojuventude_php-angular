<?php
$this->extend('Consulta.Programa/home');
$this->assign('pageHeader', 'Programas');
$this->assign('panelStyle', 'primary');
?>
<?php $this->start('bread'); ?>
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Programas</li>
</ol>
<?php $this->end(); ?>
<?php $this->start('temas'); ?>
	<div class="panel panel-default">
		<div class="panel-heading">Temas</div>
		<div class="panel-body">
			<div class="temas">
				<?php foreach($Tematicas as $Tema) { ?>
				<div data-val="<?php echo $Tema['Tematica']['id'];?>" class="tema-vagao btn-tema-programa">
					<?php echo $this->Bootstrap->title_case( $Tema['Tematica']['nome'] );?>
					<span class="badge"><?php echo count($Tema['ProgramaTematica']);?></span>
				</div>
				<?php if (!empty($Tema['children'])) { ?>
				<div class="tema-vagao hide">
					<div class="temas subtemas clearfix">
						<?php foreach($Tema['children'] as $SubTema) { ?>
						<div data-val="<?php echo $SubTema['Tematica']['id'];?>" class="tema-vagao btn-tema-programa">
							<?php echo $this->Bootstrap->title_case( $SubTema['Tematica']['nome'] );?>
							<span class="badge"><?php echo count($SubTema['ProgramaTematica']);?></span>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
		<div class="panel-footer triangulos">&nbsp;</div>
	</div>
<?php $this->end(); ?>
<?php $this->start('conteudo'); ?>

	<?php if (!empty($Programas)) { ?>
		<?php foreach($Programas as $Programa) { ?>
			<div class="panel panel-primary">
				<div class="panel-heading"><?php echo $Programa['Programa']['nome_divulgacao'];?></div>
				<div class="panel-body">
					<p><?php echo $Programa['Programa']['descricao'];?></p>
					<p><?php echo $Programa['Programa']['criterios_acesso'];?></p>
					<p><?php echo $Programa['Programa']['beneficios'];?></p>
					<br>
					<p>Início: <?php echo $Programa['OcorrenciaMunicipal']['inicio_inscricoes']; ?></p>
					<p>Fim: <?php echo $Programa['OcorrenciaMunicipal']['fim_inscricoes']; ?></p>
					<br>
					<?php echo $this->Bootstrap->btnLink('Ver mais', array(
						'action'=>'view',
						$Programa['Programa']['id']
					),
					array(
						'style' => 'success'
					)
					); ?>
				</div>
				<div class="panel-footer triangulos">&nbsp;</div>
			</div>	
		<?php } ?>
	<?php } ?>
	<?php $this->end(); ?>
	<?php $this->start('gmaps'); ?>
	<?php 
		echo $this->GoogleMapV3->map(array(
		'lat' => -15,
		'lng' => -52,
		'zoom' => 4,
		'geolocate' => true,
		'div'=>array(
			'height'=>'500', 
			'width'=>'100%',
			'id' => 'EstacaoMap'
		)
		));
 
	foreach($gMapsData as $Estado) {
		if (isset($Estado['Municipio'][0]['OcorrenciaMunicipal'])) {
			if (count($Estado['Municipio'][0]['OcorrenciaMunicipal']) > 0) {
				// add markers
				$options = array(
					'lat' => $Estado['Estado']['longitude'],
					'lng' => $Estado['Estado']['latitude'],
					'icon' => 'https://chart.googleapis.com/chart?chst=d_map_spin&chld=.7|0|99CCFF|14|b|'.count($Estado['Municipio'][0]['OcorrenciaMunicipal']),
					'content' => '<a href="/consulta/Programas/estado/'.$Estado['Estado']['id'].'">Veja os progarmas do estado de '.$Estado['Estado']['nome'].'</a>'
				);
		
				$this->GoogleMapV3->addMarker($options);
			}
		}
		

	}
 
	// print js
	echo $this->GoogleMapV3->script();
?>
<?php $this->end(); ?>

