<?php $panelStyle = ($this->fetch('panelStyle'))?($this->fetch('panelStyle')):('default'); ?>
<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-pills pull-right" role="tablist">
			<li role="presentation" class="<?php echo (isset($isLocalizacao))?('active'):('');?>"><a href="#gmaps" role="tab" data-toggle="tab">Geo-Localização</a></li>
			<li role="presentation" class="<?php echo (isset($isPrograma))?('active'):('');?>"><a href="#programas" <?php echo (isset($isPrograma))?('active'):('');?>" href="#programas" role="tab" data-toggle="tab">Pesquisa</a></li>
		</ul>
	</div>
</div>
<br>
<div class="tab-content">
	<div class="tab-pane <?php echo (isset($isLocalizacao))?('active'):('');?>" id="gmaps">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="btn-group">
					<form id="formMap" class="form-inline" method="post">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-map-marker"></span> Perto de você! <span class="caret"></span>
						</button>
						<ul id="map-btn-perto" class="dropdown-menu" role="menu">
							<li><a data-km="15" href="#">a 15 Km</a></li>
							<li><a data-km="50" href="#">a 50 Km</a></li>
							<li><a data-km="100" href="#">a 100 Km</a></li>
						</ul>
						<button type="button" class="btn btn-default" id="map-btn-estados">&nbsp;Estados</button>
						<button type="button" class="btn btn-default" id="map-btn-programas">&nbsp;Programas</button>
						<select class="form-control" name="tematicas" id="map-btn-tematicas">
							<option value="0">Escolha um Tema</option>
							<?php foreach($Tematicas as $Tema) { ?>
								<?php echo '<option value="'.$Tema['Tematica']['id'].'">'.$Tema['Tematica']['nome'].'</option>'; ?>
							<?php } ?>
						</select>
					</form>
				</div>
			</div>
			<?php echo $this->fetch('gmaps'); ?>
		</div>
	</div>
	<div class="tab-pane <?php echo (isset($isPrograma))?('active'):('');?>" id="programas">
					

<div class="row">
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">Temáticas</div>
			<div class="panel-body">
				<?php echo $this->fetch('temas'); ?>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12" id="programas-container">
				<div class="panel panel-default">
					<div class="panel-heading">Pesquise por Programas</div>
					<div class="panel-body">
						<form id="formPesquisa" method="post">
							<input type="hidden" name="data[Programa][tema_id]" id="ProgramaTemaId">
							<div class="row">
								<div class="col-md-5">
									<div class="form-group">
										<select name="data[Programa][estado_id]" class="form-control" id="pesquisar-estado">
											<option value="0">Todos os Estados</option>
											<?php foreach($Estados as $key=>$value) { ?>
											<option value="<?php echo $key;?>"><?php echo $value; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-5">
									<div class="form-group">
										<select name="data[Programa][municipio_id]" class="form-control" id="pesquisar-municipio" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">
											<option value="0">Todos os Municípios do Estado</option>
										</select>
									</div>
								</div>
								<div class="col-md-2">
									<button class="btn btn-default" id="btn-pesquisar-programas" data-title="Estação da Juventude" data-trigger="manual" data-content="Carregando..." data-placement="left">Pesquisar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php echo $this->fetch('conteudo'); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.btn-tema-programa .text').click(function(){
			if($(this).data('selected') == '1') {
				$(this).removeClass('list-group-item-info');
				$(this).data('selected', 0);
			} else {
				$(this).addClass('list-group-item-info');
				$(this).data('selected', 1);
			}
		});
		$('.show-subtema').click(function(){
			if ($('.subtemas-'+$(this).data('tema')).hasClass('slideUp')) {
				$('.subtemas-'+$(this).data('tema')).slideDown().removeClass('slideUp');
			} else {
				$('.subtemas-'+$(this).data('tema')).slideUp().addClass('slideUp');
			}
		});
		/*
		$('#btn-pesquisar-programas').click(function(){
			$('#programas-container').html('');
			$('#btn-pesquisar-programas').popover('show');
			temas = '0';
			$('span.glyphicon-ok-circle').each(function(){
				temas += ','+$(this).data('value');
			});
			$.ajax({
				'url': '/consulta/Programas/pesquisar',
				'type': 'post',
				'data': {'temas':temas,'estado':$('#pesquisar-estado').val(),'municipio':$('#pesquisar-municipio').val()},
				'success': function(data) {
					$('#btn-pesquisar-programas').popover('hide');
					$('#programas-container').html(data);
				}
			});
		});
		*/
		$('#btn-pesquisar-programas').click(function(){
			valores = [];
			$('.list-group-item-info').each(function(){
				valores.push($(this).data('val'));
			});
			$('#ProgramaTemaId').val(valores.toString());
			$('#formPesquisa').get(0).submit();
			return false;
		});
		
		$('#pesquisar-estado').change(function() {
			$('#pesquisar-municipio').popover('show');
			$.ajax({
				'url': '/consulta/Programa/municipios/'+$(this).val(),
				'type':'post',
				'success': function(data) {
					$('#pesquisar-municipio').popover('hide');
					$('#pesquisar-municipio').html(data);
				}
			})
		});
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$('#gmaps').html($('#gmaps').html());
		});
		
	});
</script>
