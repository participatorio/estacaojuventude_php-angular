<?php
class OcorrenciaMunicipal extends ConsultaAppModel {	
	public $useTable = 'ocorrencias_municipais';
	public $belongsTo = array(
		'Programa' => array(
			'className' => 'Consulta.Programa',
			'foreignKey' => 'programa_id'
		),
		'Municipio' => array(
			'className' => 'Consulta.Municipio',
			'foreignKey' => 'municipio_id'
		),
		'Situacao' => array(
			'className' => 'Consulta.Situacao',
			'foreignKey' => 'situacao_id'
		)
	);
	
	public $hasMany = array(
		'Localizacao' => array(
			'className' => 'Consulta.Localizacao',
			'foreignKey' => 'ocorrencia_municipal_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['OcorrenciaMunicipal']['inicio_inscricoes']) ) {
				$results[$key]['OcorrenciaMunicipal']['inicio_inscricoes'] = date('d/m/Y', strtotime( $value['OcorrenciaMunicipal']['inicio_inscricoes'] ) );
			}
			if ( isset($value['OcorrenciaMunicipal']['fim_inscricoes']) ) {
				$results[$key]['OcorrenciaMunicipal']['fim_inscricoes'] = date('d/m/Y', strtotime( $value['OcorrenciaMunicipal']['fim_inscricoes'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['OcorrenciaMunicipal']['inicio_inscricoes']) ) {
			$this->data['OcorrenciaMunicipal']['inicio_inscricoes'] = date_format(date_create_from_format('d/m/Y', $this->data['OcorrenciaMunicipal']['inicio_inscricoes'] ), 'Y-m-d' );
		}
		if ( !empty($this->data['OcorrenciaMunicipal']['fim_inscricoes']) ) {
			$this->data['OcorrenciaMunicipal']['fim_inscricoes'] = date_format(date_create_from_format('d/m/Y', $this->data['OcorrenciaMunicipal']['fim_inscricoes'] ), 'Y-m-d' );
		}
		return true;
	}
	
}