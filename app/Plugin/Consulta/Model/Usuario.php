<?php

class Usuario extends ConsultaAppModel {
	
	public $useTable = 'usuarios';
	
	public $order = array(
		'Usuario.nome' => 'ASC'
	);
	
	public $belongsTo = array(
	
		'Grupo' => array(
			'className' => 'Consulta.Grupo',
			'foreignKey' => 'grupo_id'
		)
		
	);
	
		
}