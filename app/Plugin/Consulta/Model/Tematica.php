<?php
class Tematica extends ConsultaAppModel {
	public $useTable = 'tematicas';
	
	public $order = array(
		'Tematica.nome' => 'ASC'
	);
	
	public $hasMany = array(
		'ProgramaTematica' => array(
			'className' => 'Consulta.ProgramaTematica',
			'foreignKey' => 'tematica_id'
		)
	);
	
	public $belongsTo = array(
		'TopTematica' => array(
			'className' => 'Consulta.Tematica',
			'foreignKey' => 'parent_id'
		)
	);
	
	// Special Actions
	
	public function listAllTematicas($conditions = array()) {
		return $this->find(
			'list',
			array(
				'fields' => array(
					'id',
					'nome'
				),
				'conditions' => $conditions
			)
		);
	}
	
}