<?php

class Grupo extends ConsultaAppModel {
	
	public $useTable = 'grupos';
	
	public $hasMany = array(
	
		'Usuario' => array(
			'className' => 'Consulta.Usuario',
			'foreignKey' => 'grupo_id'
		)
		
	);
	
		
}