<?php
class Localizacao extends ConsultaAppModel {
	public $useTable = 'localizacoes';
	public $belongsTo = array(
		'OcorrenciaMunicipal' => array(
			'className' => 'Consulta.OcorrenciaMunicipal',
			'foreignKey' => 'ocorrencia_municipal_id'
		),
		'Situacao' => array(
			'className' => 'Consulta.Situacao',
			'foreignKey' => 'situacao_id'
		),
		'Status' => array(
			'className' => 'Consulta.Status',
			'foreignKey' => 'status_id'
		)
	);
}