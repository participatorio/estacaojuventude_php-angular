<?php
class Programa extends ConsultaAppModel {
	public $useTable = 'programas';
	public $order = array(
		'Programa.nome_oficial' => 'ASC'	
	);
	public $hasMany = array(
		'ProgramaTematica' => array(
			'className' => 'Consulta.ProgramaTematica',
			'foreignKey' => 'programa_id'
		),
		'OcorrenciaMunicipal' => array(
			'className' => 'Consulta.OcorrenciaMunicipal',
			'foreignKey' => 'programa_id'
		),
		'ProgramaOrgaoExecutor' => array(
			'className' => 'Consulta.ProgramaOrgaoExecutor',
			'foreignKey' => 'programa_id'
		)
	);
	public $belongsTo = array(
		'ProgramaVinculado' => array(
			'className' => 'Consulta.Programa',
			'foreignKey' => 'programa_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Programa']['data_inicio']) ) {
				$results[$key]['Programa']['data_inicio'] = date('d/m/Y', strtotime( $value['Programa']['data_inicio'] ) );
			}
			if ( isset($value['Programa']['data_fim']) ) {
				$results[$key]['Programa']['data_fim'] = date('d/m/Y', strtotime( $value['Programa']['data_fim'] ) );
			}
		}
		}
		return $results;
	}
}