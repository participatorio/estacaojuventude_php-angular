<?php
class Estado extends ConsultaAppModel {
	
	public $useTable = 'estados';
	public $order = array(
		'Estado.nome' => 'ASC'
	);
	
	public $hasMany = array(
		'Municipio' => array(
			'className' => 'Consulta.Municipio',
			'foreignKey' => 'estado_id'
		)
	);
}