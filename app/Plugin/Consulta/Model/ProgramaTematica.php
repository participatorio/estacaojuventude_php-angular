<?php
class ProgramaTematica extends ConsultaAppModel {
	public $useTable = 'programas_tematicas';
	public $belongsTo = array(
		'Tematica' => array(
			'className' => 'Consulta.Tematica',
			'foreignKey' => 'tematica_id'
		),
		'Programa' => array(
			'className' => 'Consulta.Programa',
			'foreignKey' => 'programa_id'
		)
	);
	
}