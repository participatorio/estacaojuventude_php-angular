<?php
class OrgaoExecutor extends ConsultaAppModel {
	public $useTable = 'orgao_executor';
	public $hasMany = array(
		'ProgramaOrgaoExecutor' => array(
			'className' => 'Consulta.ProgramaOrgaoExecutor',
			'foreignKey' => 'programa_id'
		)
	);
}