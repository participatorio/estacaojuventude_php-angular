<?php
class Municipio extends ConsultaAppModel {
	public $useTable = 'municipios';
	public $order = array(
		'Municipio.nome' => 'ASC'
	);
	public $belongsTo = array(
		'Estado' => array(
			'className' => 'Consulta.Estado',
			'foreignKey' => 'estado_id'
		)
	);
		public $hasMany = array(
		'OcorrenciaMunicipal' => array(
			'className' => 'Consulta.OcorrenciaMunicipal',
			'foreignKey' => 'municipio_id'
		)
	);
}