<div class="jumbotron">
	<h1>Estação da Juventude</h1>
</div>
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading"><span class="heading">Panel 1</span></div>
			<div class="panel-body">
				Conteúdo e informações do painel 1
				<ul>
					<li><?php echo $this->Html->link('Programas por Temas', array('controller'=>'Programas','action'=>'index')); ?></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-warning">
			<div class="panel-heading"><span class="heading">Panel 2</span></div>
			<div class="panel-body">
				Conteúdo e informações do painel 2
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-danger">
			<div class="panel-heading"><span class="heading">Panel 3</span></div>
			<div class="panel-body">
				Conteúdo e informações do painel 3
			</div>
		</div>
	</div>
</div>
